import numpy as np
from sklearn import cluster

def build_inverted_index(database, codebook_size, ownership):
    """
    Builds the inverted index for the input database.
    Each sample is first linked to its nearest cluster in the codebook, then the inverted index
    is built by listing for each cluster all occurrences in the database
    :param database: A database of N samples each of dimension D (numpy array shape NxD)
    :param codebook_size: The number of clusters for the codebook
    :param ownership: A list linking each descriptor to the image it belongs to
    :return: inverted_index : A list giving for each clusters all occurrences in the database as a dictionary with
    score as value
    :return: kmeans_object : The MiniBatchKMeans object containing the coordinates of clusters
    :return: assignment_table : A numpy array giving the assigned cluster for each descriptor in database
    """

    nb_samples = database.shape[0]
    nb_images = np.unique(ownership).size
    n_clusters = codebook_size

    # Clustering
    # indices contains for each descriptor in the database the index of closest cluster
    kmeans_object = cluster.MiniBatchKMeans(n_clusters=n_clusters, verbose=True).fit(database)
    indices = kmeans_object.predict(database)

    tf = np.zeros((nb_images, n_clusters))
    counts = np.zeros_like(tf)
    # each line of counts sums the occurrences of each word

    for i in range(nb_images):
        current_image_descriptors = indices[np.asarray(ownership) == i]
        total_per_cluster = np.bincount(current_image_descriptors, minlength=codebook_size)
        tf[i, :] = total_per_cluster / np.sum(total_per_cluster)
        counts[i, :] += total_per_cluster

    totalocc = np.sum((counts >= 1), axis=0)
    idf = np.log(np.divide(nb_images * np.ones_like(totalocc), totalocc + 1))

    scores = tf * idf

    # At this point we have the clusters, the TF-IDF score of each image
    # To build the inverted index, for each cluster we will create a dict {id image : weight} linking to the occurrences
    # scores = matrix of shape (nb_img, n_clusters)

    # Index will contain list of occurences with scores for each cluster
    index = []  # type: ndarray

    for visualword in range(n_clusters):
        index.append({})
        for img in range(nb_images):
            if scores[img, visualword] != 0:
                index[visualword].update({img: np.float16(scores[img, visualword])})

    return index, kmeans_object, indices