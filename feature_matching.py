from scipy.spatial import cKDTree
from skimage.measure import ransac

def match_features(descriptorsA, descriptorsB, distance_threshold):
    """
    Matches features from two images using a KD tree.
    :param descriptorsA descriptors from image A
    :param descriptorsB descriptors from image B
    :param distance_threshold maximum distance for KDtree neighbours
    :return: indices of matching descriptions
    """

    # Find nearest-neighbor matches using a KD tree.
    d1_tree = cKDTree(descriptorsA)
    _, indices = d1_tree.query(
        descriptorsB, distance_upper_bound=distance_threshold)

    return indices


def verify_RANSAC(locationsA, locationsB, transform, get_model=False):
    """
    Performs a geometric verification of coherence for locations from image A and locations from image B,
    with respect to the transformation supplied as a parameter
    :param locationsA: list of locations in image A
    :param locationsB: list of locations in image B (must be the same size as locationsA)
    :param transform: skimage transform, transformation assumed between the two images
    :return: inliers, list of inliers for this hypothesis
    """
    model, inliers = ransac(
            (locationsA, locationsB),
            transform,
            min_samples=3,
            residual_threshold=20,
            max_trials=1000)

    if get_model:
        return model, inliers
    else:
        return inliers
