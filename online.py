from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import time
import os
import pickle
import json
import numpy as np
from skimage.measure import ransac
from skimage.transform import AffineTransform
import tensorflow as tf
from util import input_fn, read_image_paths, display_correspondences, extract_features
from feature_io import ReadAllFromFile
from protos import delf_config_pb2
from google.protobuf import text_format
import feature_extractor
from feature_matching import match_features, verify_RANSAC
import faiss

conf = None

_DISTANCE_THRESHOLD = 0.8


def find_most_similar(conf, verbose=True, nb=1, k=3):
    tf.logging.set_verbosity(tf.logging.INFO)

    # Read features for the image base
    basefeatures = ReadAllFromFile(conf.basefeat_path)
    num_images_base = len(basefeatures)

    locations = []
    for i, descriptor in enumerate(basefeatures):
        [base_locations, _, _, _, _, _] = descriptor
        locations.extend(base_locations)
    locations = np.asarray(locations)

    # Read list of source images for display
    baseimages = read_image_paths(conf.base_path)
    if verbose:
        tf.logging.info("Loaded %d images to compare with request" % num_images_base)

    # Get index
    index = faiss.read_index("data/faiss_indexes/ivfpq.index")

    with open("data/index_ownership.json", "r") as read_file:
        index_ownership = np.array(json.load(read_file))

    # Check if size is correct
    assert len(baseimages) == num_images_base
    assert index_ownership.shape[0] == len(locations)

    print("Starting a nearest neighbour search with request", conf.request_path)

    # First we extract the features for the request image
    request_features = extract_features(conf, [conf.request_path])
    [locations_out, feature_scales_out, descriptors_out, attention_out, _] = request_features[0]

    base = dict(locations=locations, ownership=index_ownership, basepaths=baseimages, index=index)
    request = dict(locations=locations_out, descriptors=descriptors_out)

    ANN_search(base=base, request=request, k=k, useRANSAC=True)


def find_most_similar_benchmark(locations, descriptors, index_ownership, baseimages, index,
                                 request=1, k=3, useRANSAC=False):
    # Same as find_most_similar but with request descriptor already included in the base

    print("Starting a nearest neighbour search with request #", request)

    # Getting features for request
    locations_out = locations[index_ownership == request]
    descriptors_out = descriptors[index_ownership == request]

    base = dict(locations=locations, ownership=index_ownership, basepaths=baseimages, index=index)
    request = dict(locations=locations_out, descriptors=descriptors_out)

    return ANN_search(base, request, k, useRANSAC)


def ANN_search(base, request, k, useRANSAC):
    """
    Performs an approximate nearest neighbours search.
    :param base: Dictionary containing :
        locations : list of locations of the descriptors in the base
        ownership : list of ownership for each descriptor (which image it belongs to)
        basepaths : full paths of images in the base
        index : IVFPQ index
    :param request: Dictionary containing :
        locations : list of locations of the descriptors in the request
        descriptors : list of descriptors in the request
    :param k: number of base descriptors to consider when establishing list of potential matches
    :param useRANSAC: geometric verification of an AffineTransform with RANSAC
    :return: ordered list of images in the base + corresponding scores
    """

    # Size of the databse
    num_images_base = len(base["basepaths"])

    # Figuring out the number N of descriptors extracted
    nb_descriptors = len(request["descriptors"])

    matched_descriptors = np.zeros((nb_descriptors, k), dtype=int)  # stores all matches
    distances = np.zeros_like(matched_descriptors, dtype=float)  # stores the distances between request and matches

    index = base["index"]

    for i, descriptor in enumerate(request["descriptors"]):

        # Get K nearest descriptors in database
        distances[i, :], matched_descriptors[i, :] = index.search(np.array([descriptor]), k)

    # Link each descriptor to the image it belongs to
    # corresponding_images is a numpy array of shape N x K
    # (N : Number of descriptors extracted in the request)

    corresponding_images = base["ownership"][matched_descriptors]

    sumofinliers = np.zeros(num_images_base)

    if not useRANSAC:
        candidate_images, nboccurences = np.unique(corresponding_images, return_counts=True)
        for i, candidate in enumerate(candidate_images):
            sumofinliers[candidate] = nboccurences[i]

        winners = np.argsort(sumofinliers)[::-1].astype(int)
        print("The closest match is", base["basepaths"][winners[0]])
        return winners, sumofinliers

    if useRANSAC:
        # Now for each image, try to fit a AffineTransform model using RANSAC and store the number of inliers
        # as the score for this image
        candidate_images = np.unique(corresponding_images)
        for candidate in candidate_images:

            # Select feature locations for putative matches.
            #  Get locations of matched descriptors belonging to candidate image :
            #  If a descriptor from request has been matched to two or more different descriptors in candidate image,
            # duplicate request descriptor as many times as needed
            flattened_correponding_images = np.reshape(corresponding_images, nb_descriptors * k)
            flattened_best_matches = np.reshape(matched_descriptors, nb_descriptors * k)
            candidate_locations = base["locations"][flattened_best_matches[flattened_correponding_images == candidate]]
            # candidate_locations is a list of the locations of descriptors in image candidate that have been matched

            number_of_matches = np.sum(corresponding_images == candidate, axis=1)
            request_locations = []
            for i, count in np.ndenumerate(number_of_matches):
                if count == 0:
                    continue
                if count == 1:
                    request_locations.append(request["locations"][i])
                if count > 1:
                    request_locations.append(request["locations"][i])
                    for _ in range(count - 1):
                        request_locations.append(np.array([0, 0]))

            request_locations = np.asarray(request_locations)
            # Perform geometric verification using RANSAC.
            if candidate_locations.shape[0] == 0 or request_locations.shape[0] == 0 or candidate_locations.size == 0 \
                    or request_locations.size == 0:
                continue

            else:
                try:
                    inliers = verify_RANSAC(candidate_locations, np.asarray(request_locations), AffineTransform)
                except np.linalg.LinAlgError:
                    # Handling unexpected errors during RANSAC verification
                    print("Warning! LinAlgError encountered while processing image" + base["basepaths"][candidate])
                    continue

            if inliers is None:
                continue

            sumofinliers[candidate] = sum(inliers)

        winners = np.argsort(sumofinliers)[::-1].astype(int)

        print("The closest match is", base["basepaths"][winners[0]])

        return winners, sumofinliers

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config_path',
        type=str,
        default='delf_config_example.pbtxt',
        help="""
        Path to DelfConfig proto text file with configuration to be used for DELF extraction.""")
    parser.add_argument(
        '--request_path',
        type=str,
        default='test_images/image_1.jpg',
        help="""
      Path to test image 1.
      """)
    parser.add_argument(
        '--base_path',
        type=str,
        default='test_images/imagelist.txt',
        help="""
      Path to text file containing base images for displaying matches.
      """)
    parser.add_argument(
        '--basefeat_path',
        type=str,
        default='test_features/',
        help="""
      Path to file containing DELF features for the image base.
      """)
    args, unparsed = parser.parse_known_args()
    find_most_similar(args)
