from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps
from util import input_fn, write_image_paths
import argparse
import os
import json
import sys
import time
import tensorflow as tf
import feature_io
from feature_io import ReadAllFromFile, ReadFromFile
from util import input_fn, read_image_paths, extract_and_save, getrecursivefilelist
import faiss
import random
import pandas as pd
import pickle


_DELF_EXT = '.delf'
_STATUS_CHECK_ITERATIONS = 5


def show_images(image_path_list):
    plt.figure()
    for i, image_path in enumerate(image_path_list):
        plt.subplot(1, len(image_path_list), i+1)
        plt.imshow(np.asarray(Image.open(image_path)))
        plt.title(image_path)
        plt.grid(False)
        plt.yticks([])
        plt.xticks([])
    plt.show()


def extract_all_descriptors(conf):
    """
    Extraction of the descriptors over a database (the number of descriptor per image may vary).
    The database of descriptors is treated with L2-normalization + PCA + L2-normalization to reduce dimension
    :param conf : should contain :
        path : path to the top folder of the database (with subfolders for each class)
        features_path : path to the top folder of the database (with subfolders for each class)

    """
    tf.logging.set_verbosity(tf.logging.INFO)

    # Read list of images.
    tf.logging.info('Reading list of images...')
    image_paths = input_fn(conf.path, '.jpg')
    num_images = len(image_paths)
    tf.logging.info('done! Found %d images', num_images)

    # Create output directory if necessary.
    if not os.path.exists(conf.features_path):
        os.makedirs(conf.features_path)

    if conf.reverse:
        image_paths.reverse()

    index = extract_and_save(conf, image_paths, conf.features_path)

    # Write list of images to a file
    # write_image_paths(image_paths, conf.output_dir + "/imagelist.txt")


def index_database(conf):
    """
    Process the database to produce the index and files for a request of nearest neighbours
    The pipeline is as follows :
    - extraction of the descriptors for all the images (the number of descriptor per image may vary)
    - the database of descriptors is treated with L2-normalization + PCA + L2-normalization to reduce dimension
    - the database is clustered to get a codebook and build an inverted index linking each word to the images it occurs in
    - for each word of the codebook, the set of descriptors belonging to it is product-quantized
    :param unused_argv:
    :return:
    """

    # Read features for the image base
    basefeatures = ReadAllFromFile(conf.path)

    index_features = []
    index_locations = []
    index_ownership = []

    for i, descriptor in enumerate(basefeatures):
        [locations_out,
        feature_scales_out, descriptors_out,
        attention_out, _, num_features] = descriptor
        index_features.extend(descriptors_out)
        index_locations.append(locations_out)
        index_ownership.extend(num_features*[i])

    index_features = np.array(index_features).astype('float32')
    index_locations = np.array(index_locations)
    index_ownership = np.array(index_ownership)

    coarse_quantizer = faiss.IndexFlatL2(40)
    index = faiss.IndexIVFPQ(coarse_quantizer, 40, 100, 10, 3)
    index.train(index_features)
    index.add(index_features)
    faiss.write_index(index, "data/ivfpq.index")

    with open("data/index_ownership.json", "w") as write_file:
        json.dump(index_ownership.tolist(), write_file)

    #TODO: store the database of descriptors as bit codes with protobuf


def assign_to_clusters(conf):
    """
    Reads DELF features from DELF, performs KMeans clustering and writes assigned clusters to disk for each image
    """

    # Read features for the image base
    featlist = getrecursivefilelist(conf.path, extensions=['.delf'])

    features = []
    ownership = []

    for i, featpath in enumerate(featlist):
        descriptor = ReadFromFile(featpath)
        [locations_out,
        feature_scales_out, descriptors_out,
        attention_out, _] = descriptor
        features.extend(descriptors_out)
        if i % 1000 == 0:
            print("{} done".format(i))
        ownership.extend(descriptors_out.shape[0]*[os.path.splitext(os.path.split(featpath)[1])[0]])

    features_trainset = np.array(random.sample(features, 2500000)).astype('float32')
    features = np.array(features).astype('float32')

    kmeans = faiss.Kmeans(d=40, k=1000, niter=20, verbose=True)
    kmeans.train(features_trainset)

    D, I = kmeans.index.search(features, 1)

    with open('assigned_clusters.pkl', 'wb') as f:
        pickle.dump(I, f)

    with open('ownership.pkl', 'wb') as f:
        pickle.dump(ownership, f)

    qsdqsd = 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compute DELF descriptors for images in a directory')
    parser.add_argument(
        '--config_path',
        type=str,
        default='delf_config_example.pbtxt',
        help="""
        Path to DelfConfig proto text file with configuration to be used for DELF extraction.""")
    parser.add_argument(
        '--path',
        type=str,
        required=True,
        help="""
        Directory containing the images whose DELF features will be extracted.""")
    parser.add_argument(
        '--features_path',
        type=str,
        default='test_features',
        help="""
        Directory where DELF features will be written to"""
    )
    parser.add_argument(
        '--extract',
        default=False,
        action='store_true',
        help="""
              Extract the descriptors if True, index otherwise
              """)
    parser.add_argument(
        '--reverse',
        type=bool,
        default=False,
        help="""
              Inverse direction when parsing list of files (for multithreading)
              """)
    conf, unparsed = parser.parse_known_args()
    from tensorflow.python.client import device_lib

    print(device_lib.list_local_devices())
    paths = getrecursivefilelist(conf.path, extensions=[".jpg"])
    extract_and_save(conf, paths, conf.features_path)
    # refdf = pd.read_pickle("/data/dgominski/googleLandmarks/glbbsdataframe.pkl")
    # assignment = pickle.load(open('assigned_clusters.pkl', "rb"))
    # ownership = np.asarray(pickle.load(open('ownership.pkl', "rb")))
    # unique_ids = np.unique(ownership)
    # df = pd.DataFrame(columns=['clusters', 'class'], index=unique_ids)
    #
    # for i, unique_id in enumerate(unique_ids):
    #     clusters = assignment[np.nonzero(ownership == unique_id)].tolist()
    #     clusters = [item for sublist in clusters for item in sublist]
    #     df.at[unique_id, 'clusters'] = ' '.join(map(str, clusters))
    #     try:
    #         ref = refdf[refdf['id'] == unique_id].iloc[0]
    #         df.at[unique_id, 'class'] = int(ref['class'])
    #     except:
    #         df.at[unique_id, 'class'] = None
    #     if i % 100 == 0:
    #         print(i)
    #
    # df.to_pickle("glandmarksclustered.pkl")
    # df = pd.read_pickle("/data/dgominski/googleLandmarks/glbbsdataframe.pkl")
    # assign_to_clusters(conf)



