import numpy as np
from util import generate_ground_truth
from os import listdir
import csv
from sklearn import metrics

INPUT_FOLDER = "/home/dimitri/projets/proto/resultsGrayscale10"

# First compute ground-truth
book_of_truth = generate_ground_truth("/home/dimitri/Pictures/datasets/testALEGORIA_grayscale", ".jpg")

# Read files of results, create N lists, one for each request
book_of_results = {}
for i in range(1, 42):
    book_of_results[str(i).zfill(2)] = {}
list_of_result_files = listdir(INPUT_FOLDER)

for file in list_of_result_files:
    book_of_results[file[:2]][file[3:7]] = []
    f = open(INPUT_FOLDER+'/'+file, 'r')
    for line in f.readlines():
        # discard scores
        line = line[:7]
        book_of_results[file[:2]][file[3:7]].append(line)

number_of_images = len(book_of_results['01']['0001'])

# Now for each class
# and for each value of k tested (varying along classes)
# and for each request
# check for each output if it is in the ground truth

mAP = []

for c in range(1, 42):
    SUM2 = 0
    number_of_images_in_class = len(book_of_truth[str(c).zfill(2)])
    for request in range(1, number_of_images_in_class + 1):
        retrieved = book_of_results[str(c).zfill(2)][str(request).zfill(4)][:number_of_images_in_class]
        ground_truth = book_of_truth[str(c).zfill(2)]

        positives = 0
        SUM1 = 0
        for i, output in enumerate(retrieved):
            relevance = output in ground_truth
            positives += relevance
            SUM1 += positives * relevance

        aSUM1 = SUM1 / number_of_images_in_class

    SUM2 += aSUM1

    mAP.append((1 / number_of_images_in_class) * SUM2)

print(mAP)

with open('results.csv', 'w') as file:
    writer = csv.writer(file, delimiter=',')
    writer.writerow(mAP)