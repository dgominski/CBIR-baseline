from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import time
import os
import pickle
import json
import numpy as np
from skimage.measure import ransac
from skimage.transform import AffineTransform
import tensorflow as tf
from util import input_fn, read_image_paths, display_correspondences, extract_features
from feature_io import ReadAllFromFile, ReadFromFile
from protos import delf_config_pb2
from google.protobuf import text_format
import feature_extractor
from feature_matching import match_features, verify_RANSAC
import faiss
import glob
import pandas as pd
from evaluate import compute_map
import index


def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

dataroot = "/data/dgominski/alegoria_v2/"

csvpath = os.path.join(dataroot, "data.csv")
df = pd.read_csv(csvpath)
df['path'] = df.apply(lambda x: os.path.join(dataroot, "queries", x['classname'].split(',')[0], x['filename']), axis=1)

distractorsfolder = os.path.join(dataroot, "distractors")
subfolders = sorted(get_immediate_subdirectories(distractorsfolder))
distractors = []
for folder in subfolders:
    distractors.extend(glob.glob(os.path.join(distractorsfolder, folder, "*.jpg")))

df['class'] = df['class'].apply(lambda x: [int(q) for q in str(x).split(',')])
df = df.append(pd.DataFrame({'path': distractors}), ignore_index=True)

queriespath = os.path.join("/data/dgominski/alegoria_v2", "queries.csv")
queries = pd.read_csv(queriespath)
queries['path'] = queries.apply(lambda x: os.path.join(dataroot, "queries", x['classname'].split(',')[0], x['filename']), axis=1)
queries['positive'] = queries['positive'].apply(lambda x: x.strip('[').strip(']').split(','))
gt = queries.to_dict(orient='index')

CODEBOOK_SIZE = 256

# READ DELF FEATURES FROM DISK
features = []
ownership = []
errors = []
for i, row in df.iterrows():
    filename = row['filename'].strip(".jpg")
    delf_name = filename + ".delf"
    fullpath = os.path.join(dataroot, "delf_features/"+delf_name)
    if not os.path.exists(fullpath):
        errors.append(i)
        continue
    basefeatures = ReadFromFile(fullpath)[2]
    features.extend(basefeatures)
    ownership.extend(basefeatures.shape[0]*[i])

features = np.array(features)
ownership = np.array(ownership)

index = index.IndexIVFPQ(codebook_size=CODEBOOK_SIZE)
index.train(features, ownership)

listranks = []
matchlist = []
for i, row in queries.iterrows():
    query_features = features[ownership==i]
    if query_features.size == 0:
        listranks.append(np.zeros(len(queries.index)))
        matchlist.append(np.zeros(10))
        continue
    ranks, similarity = index.search(query_features)
    listranks.append(ranks)
    paths = queries['filename'].to_numpy()[ranks][:10]
    matchlist.append(paths)

matchdf = pd.DataFrame({'query': queries['filename'], 'results': matchlist})
matchdf.to_csv("matchlist.csv")

ranks = np.array(listranks)
map, aps, pr, prs = compute_map(ranks, gt)
print('mAP with codebook size {}: {}'.format(CODEBOOK_SIZE, map))
print('prec@ [1 5 10]: {}'.format(pr))


