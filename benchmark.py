import argparse
import pickle
from feature_io import ReadAllFromFile, ReadFromFile, DelfFeaturesToArrays
import numpy as np
import json
import csv
import os
import pandas as pd
import glob
#from util import input_fn #extract_and_save, get_naming_function
import faiss
from testdataset import generate_GT_from_imagesubfolders, configdataset, googleLandmarksBB, birds
from evaluate import compute_map_per_class, compute_map_and_print, compute_map
from feature_matching import match_features, verify_RANSAC
from skimage.transform import AffineTransform, warp
import time
from PIL import Image
import ast
from index import IndexIVFPQ, IndexASMK
from match_two import match_two_no_plot

def benchmark_from_folder(path, cfg, extractfeatures=False, useRANSAC=False):
    """Computes the mAP per class using a folder containing all the features and the dataset conf with the ground-truth.
    gnd = {qidx : list of indexes of the query in the full list of images imlist
    gnd : list of dictionnaries with key "ok" containing a list of all positive images
    imlist : list of all the images
    qimlist : list of all the queries
    classes : classes of the images}
    If extractfeatures set to True, first extract the features
    """
    # Dimensionality of the descriptor
    d = 40
    # Get k closest descriptors
    k = 60

    if extractfeatures:
        conf = argparse.Namespace(config_path="config/delf_config_example.pbtxt")
        print("Preparing to extract and save the features from {} images to output folder {}"
              .format(len(cfg['imlist']), path))
        extract_and_save(conf, cfg['imlist'], path)

    # First load and check imlist and features : each image must have an associated delf file
    features = []
    for image in cfg['imlist']:
        idimage = image.split("/")[-1]
        featurepath = os.path.join(path, idimage + ".delf")
        if not os.path.exists(featurepath):
            print("Feature {} was not found".format(featurepath))
        features.append(featurepath)

    nb_images = len(cfg['imlist'])

    # Objective : get for each request image the similarity with every image in the base, by decreasing order
    ranks = np.zeros((len(cfg['qidx']), nb_images), dtype=np.int)

    # Deserialize the features into a numpy array

    ownership = []

    index = faiss.IndexPQ(d, 16, 8)
    res = faiss.StandardGpuResources()
    index = faiss.index_cpu_to_gpu(res, 0, index)

    for i, feat in enumerate(features[:200]):
        feat = ReadFromFile(feat)
        if i == 0:
            descriptors = np.asarray(feat[2]).astype('float32')
        else:
            descriptors = np.concatenate((descriptors, np.asarray(feat[2]).astype('float32')), axis=0)

    index.train(descriptors)
    print("index trained")

    for i, feat in enumerate(features):
        feat = ReadFromFile(feat)
        if feat[0].size == 0:
            print("No descriptors in delf features {}".format(cfg['imlist'][i]))
            descriptors.append(-1 * np.ones((1, d)))
            ownership.extend([i])
        else:
            ownership.extend(feat[2].shape[0] * [i])
            #descriptors = np.concatenate((descriptors, ), axis=0).astype('float32')
            index.add(np.asarray(feat[2]).astype('float32'))

        if i % 1000 == 0:
            print("Treated {} sets of image descriptors out of {}".format(i, nb_images))

    ownership = np.asarray(ownership)

    # coarse_quantizer = faiss.IndexFlatL2(d)
    # index = faiss.IndexIVFPQ(coarse_quantizer, d, 8000, 10, 8)
    index.nprobe = 60

    faiss.write_index(index, "data/indexflatl2.index")

    start = time.time()

    for j, request in enumerate(cfg['qidx']):

        if j % 10 == 0:
            print("Treated {} request images out of {}".format(j, len(cfg['qidx'])))

        # Get the request descriptors
        #requestdescriptors = descriptors[ownership == request]
        requestdescriptors = ReadFromFile(os.path.join("/data/dgominski/testALEGORIA", "features", cfg['imlist'][request].split("/")[-1]) + ".delf")[2].astype('float32')
        nb_requestdescriptors = requestdescriptors.shape[0]

        #requestlocations = locations[ownership == request]

        # Get K nearest descriptors in database
        distances, matched_descriptors = index.search(requestdescriptors, k)

        corresponding_images = ownership[matched_descriptors]

        sumofinliers = np.zeros(nb_images)

        if not useRANSAC:
            candidate_images, nboccurences = np.unique(corresponding_images, return_counts=True)
            for i, candidate in enumerate(candidate_images):
                sumofinliers[candidate] = nboccurences[i]

        if useRANSAC:
            # Now for each image, try to fit a AffineTransform model using RANSAC and store the number of inliers
            # as the score for this image
            candidate_images = np.unique(corresponding_images)
            for candidate in candidate_images:

                # Select feature locations for putative matches.
                # Get locations of matched descriptors belonging to candidate image :
                # If a descriptor from request has been matched to two or more different descriptors in candidate image,
                # duplicate request descriptor as many times as needed
                flattened_correponding_images = np.reshape(corresponding_images, nb_requestdescriptors * k)
                flattened_best_matches = np.reshape(matched_descriptors, nb_requestdescriptors * k)
                candidate_locations = locations[flattened_best_matches[flattened_correponding_images == candidate]]
                # candidate_locations is a list of the locations of descriptors in image candidate that have been matched

                number_of_matches = np.sum(corresponding_images == candidate, axis=1)
                request_locations = []
                for i, count in np.ndenumerate(number_of_matches):
                    if count == 0:
                        continue
                    if count == 1:
                        request_locations.append(requestlocations[i])
                    if count > 1:
                        request_locations.append(requestlocations[i])
                        for _ in range(count - 1):
                            request_locations.append(np.array([0, 0]))

                request_locations = np.asarray(request_locations)
                # Perform geometric verification using RANSAC.
                if candidate_locations.shape[0] == 0 or request_locations.shape[0] == 0 or candidate_locations.size == 0 \
                        or request_locations.size == 0:
                    continue

                else:
                    try:
                        inliers = verify_RANSAC(candidate_locations, np.asarray(request_locations), AffineTransform)
                    except np.linalg.LinAlgError:
                        # Handling unexpected errors during RANSAC verification
                        print("Warning! LinAlgError encountered while processing image")
                        continue

                if inliers is None:
                    continue

                sumofinliers[candidate] = sum(inliers)

        ranks[j, :] = np.argsort(sumofinliers)[::-1].astype(int)

    ranks = ranks.T

    compute_map_and_print('alegoria', ranks, cfg['gnd'])
    print(compute_map_per_class(ranks, cfg['gnd'], cfg['classes'])[0])

    end = time.time()
    print("search took {} seconds".format(end - start))


def benchmark_orb_from_folder(path, cfg):
    """Computes the mAP per class using a folder containing all the features and the dataset conf with the ground-truth.
    gnd = {qidx : list of indexes of the query in the full list of images imlist
    gnd : list of dictionnaries with key "ok" containing a list of all positive images
    imlist : list of all the images
    qimlist : list of all the queries
    classes : classes of the images}
    If extractfeatures set to True, first extract the features
    """
    # Dimensionality of the descriptor
    d = 32
    # Get k closest descriptors
    k = 60

    # First load and check imlist and features : each image must have an associated delf file
    features = []
    for image in cfg['imlist']:
        idimage = image.split("/")[-1]
        featurepath = os.path.join(path, idimage + ".npy")
        if not os.path.exists(featurepath):
            print("Feature {} was not found".format(featurepath))
        features.append(featurepath)

    nb_images = len(cfg['imlist'])

    # Objective : get for each request image the similarity with every image in the base, by decreasing order
    ranks = np.zeros((len(cfg['qidx']), nb_images), dtype=np.int)

    # Deserialize the features into a numpy array

    descriptors = []
    ownership = []

    for i, feat in enumerate(features):
        feat = np.load(feat)
        if feat[0].size == 0:
            print("No descriptors in np file {}".format(cfg['imlist'][i]))
            descriptors.append(-1 * np.ones((1, d)))
            ownership.extend([i])
        else:
            descriptors.append(feat)
            ownership.extend(feat.shape[0] * [i])

        if i % 1000 == 0:
            print("Treated {} sets of image descriptors out of {}".format(i, nb_images))

    descriptors = np.concatenate(descriptors, axis=0).astype('float32')
    ownership = np.asarray(ownership)

    index = faiss.IndexFlatL2(d)
    # coarse_quantizer = faiss.IndexFlatL2(d)
    # index = faiss.IndexIVFPQ(coarse_quantizer, d, 8000, 10, 8)
    res = faiss.StandardGpuResources()
    index = faiss.index_cpu_to_gpu(res, 0, index)
    index.train(descriptors)
    index.add(descriptors)
    index.nprobe = 60

    #faiss.write_index(index, "data/indexflatl2.index")

    start = time.time()

    for j, request in enumerate(cfg['qidx']):

        if j % 10 == 0:
            print("Treated {} request images out of {}".format(j, len(cfg['qidx'])))

        # Get the request descriptors
        requestdescriptors = descriptors[ownership == request]
        nb_requestdescriptors = requestdescriptors.shape[0]

        # Get K nearest descriptors in database
        distances, matched_descriptors = index.search(requestdescriptors, k)

        corresponding_images = ownership[matched_descriptors]

        sumofinliers = np.zeros(nb_images)

        candidate_images, nboccurences = np.unique(corresponding_images, return_counts=True)
        for i, candidate in enumerate(candidate_images):
            sumofinliers[candidate] = nboccurences[i]

        ranks[j, :] = np.argsort(sumofinliers)[::-1].astype(int)

    ranks = ranks.T

    compute_map_and_print('alegoria', ranks, cfg['gnd'])
    mapperclass = compute_map_per_class(ranks, cfg['gnd'], cfg['classes'])[0]

    end = time.time()
    print("search took {} seconds".format(end - start))


def get_correspondences(df, featpath, buildindex=False):
    """Get retrieval results for all images in dataframe df. Features for all images must be in folder featpath, where
    image x.jpg has correponding DELF feature x.delf
    """
    # Dimensionality of the descriptor
    d = 40
    # Get k closest descriptors
    k = 500
    # Keep n closest images
    n = 10

    # First load and check imlist and features : each image must have an associated delf file
    features = []
    count = 0
    for image in df['filename']:
        idimage, _ = os.path.splitext(image)
        featurepath = os.path.join(featpath, idimage + ".delf")
        if not os.path.exists(featurepath):
            count += 1
            continue
        features.append(featurepath)
    print("{} feature files were not found".format(count))

    nb_images = len(df.index)

    # Deserialize the features into a numpy array

    ownership = []

    if buildindex:
        coarse_quantizer = faiss.IndexFlatL2(d)
        ncentroids, code_size = (80000, 8)
        index = faiss.IndexIVFPQ(coarse_quantizer, d,
                                 ncentroids, code_size, 8)
        res = faiss.StandardGpuResources()
        index = faiss.index_cpu_to_gpu(res, 0, index)

        for i, feat in enumerate(features[:10000]):
            feat = ReadFromFile(feat)
            if i == 0:
                descriptors = np.asarray(feat[2]).astype('float32')
            else:
                descriptors = np.concatenate((descriptors, np.asarray(feat[2]).astype('float32')), axis=0)

        index.train(descriptors)
        print("index trained")

        for i, feat in enumerate(features):
            # 0: location, 1: scale, 2: descriptor, 3: attention score
            feat = ReadFromFile(feat)
            if feat[0].size == 0:
                print("No descriptors in delf feature file {}".format(df.iloc[i]['imlist']))
                ownership.extend([i])
            else:
                ownership.extend(feat[2].shape[0] * [i])
                #descriptors = np.concatenate((descriptors, ), axis=0).astype('float32')
                index.add(np.asarray(feat[2]).astype('float32'))

            if i % 1000 == 0:
                print("#INDEXBUILDING : Treated {} sets of image descriptors out of {}".format(i, nb_images))

        ownership = np.asarray(ownership)

        index.nprobe = 60

        faiss.write_index(faiss.index_gpu_to_cpu(index), "data/indexbirds.index")
        np.save("data/ownership", ownership)

    else:
        index = faiss.read_index("data/indexbirds.index")
        ownership = np.load("data/ownership.npy")
        res = faiss.StandardGpuResources()
        index = faiss.index_cpu_to_gpu(res, 0, index)

    start = time.time()

    results = pd.DataFrame(data=None, columns=df.columns)

    for j, request in enumerate(df['filename']):

        if j % 10 == 0:
            print("#IMAGESEARCH : Treated {} request images out of {}".format(j, len(df.index)))

        # Get the request descriptors
        if not os.path.exists(os.path.join(featpath, os.path.splitext(df.iloc[j]['filename'])[0] + '.delf')):
            print("Feature not found")
            continue
        # 0: location, 1: scale, 2: descriptor, 3: attention score
        feat = ReadFromFile(os.path.join(featpath, os.path.splitext(df.iloc[j]['filename'])[0] + '.delf'))
        requestlocations = feat[0]
        requestdescriptors = feat[2].astype('float32')
        requestdescriptorsnum = requestdescriptors.shape[0]

        #requestlocations = locations[ownership == request]

        # Get K nearest descriptors in database
        distances, matched_descriptors = index.search(requestdescriptors, k)

        corresponding_images = ownership[matched_descriptors]

        sumofinliers = np.zeros(nb_images)

        candidate_images, nboccurences = np.unique(corresponding_images, return_counts=True)
        for i, candidate in enumerate(candidate_images):
            sumofinliers[candidate] = nboccurences[i]

        ranks = np.argsort(sumofinliers)[::-1].astype(int)

        # First check the class to eliminate false positives
        checked_candidates = []
        request_class = df.iloc[j]['class']
        # Get images with the same class
        sameclass = df.loc[df['class'] == request_class]
        # Keep only images with at least n inliers
        sameclassinliers = sumofinliers[sameclass.index.to_numpy()]
        checked_candidates = sameclass.iloc[sameclassinliers[1:] > 3]

        if checked_candidates.empty:
            continue

        # Now for each image, try to fit a AffineTransform model using RANSAC and store the number of inliers
        # as the score for this image
        for m, (idx, row) in enumerate(checked_candidates.iterrows()):
            if not os.path.exists(os.path.join(featpath, os.path.splitext(df.iloc[idx]['filename'])[0] + '.delf')):
                print("Feature not found")
                continue
            # 0: location, 1: scale, 2: descriptor, 3: attention score
            feat = ReadFromFile(os.path.join(featpath, os.path.splitext(df.iloc[idx]['filename'])[0] + '.delf'))
            candidatelocations = feat[0]
            candidatedescriptors = feat[2]
            num_features_2 = candidatelocations.shape[0]

            indices = match_features(requestdescriptors, candidatedescriptors, 0.8)

            # Select feature locations for putative matches.
            locations_2_to_use = np.array([
                candidatelocations[f,]
                for f in range(num_features_2)
                if indices[f] != requestdescriptorsnum
            ])
            locations_1_to_use = np.array([
                requestlocations[indices[f],]
                for f in range(num_features_2)
                if indices[f] != requestdescriptorsnum
            ])

            # Perform geometric verification using RANSAC.
            try:
                model, inliers = verify_RANSAC(locations_1_to_use, locations_2_to_use, AffineTransform, get_model=True)
            except ValueError:
                continue

            if inliers is None:
                print("No coherent geometric transformation found !")
                continue
            else:
                tmpresult = df.iloc[j].copy()
                tmpresult["ref"] = row['filename']
                tmpresult["numinliers"] = len(inliers)
                tmpresult["params"] = model.params
                results = results.append(tmpresult)

    end = time.time()
    print("search took {} seconds".format(end - start))

    results.to_csv("data/resultsbirds.csv")


def benchmark_on_dataframe(df, queriesdf, feats_dir, gnd, index=None, useRANSAC=False):
    """Compute mAP on the retrieval dataset given in the dataframe
    """
    #df = df.iloc[:100]
    #queriesdf = queriesdf.iloc[:200]
    # Get k closest descriptors
    k = 500
    # Keep n closest images
    n = 10

    if index is None:
        # First load and check imlist and features : each image must have an associated delf file
        feature_paths = []
        for image in df['path']:
            _, idimage = os.path.split(image)
            idimage, _ = os.path.splitext(idimage)
            featurepath = os.path.join(feats_dir, idimage + ".delf")
            if not os.path.exists(featurepath):
                print(featurepath)
                raise ValueError
            feature_paths.append(featurepath)

        print("Found {} images with corresponding .delf files".format(len(feature_paths)))

        nb_images = len(df.index)

        feats = []
        nb_features = []
        for i, feat in enumerate(feature_paths):
            # 0: location, 1: scale, 2: descriptor, 3: attention score
            feat = ReadFromFile(feat)
            if feat[0].size == 0:
                print("No descriptors in delf feature file {}".format(df.iloc[i]['imlist']))
            else:
                npfeats = np.asarray(feat[2].astype('float32'))
                feats.append(npfeats)
                nb_features.append(npfeats.shape[0])

            if i % 100 == 0:
                print("#INDEXBUILDING : Loaded {} sets of image descriptors out of {}".format(i, nb_images))

        index = IndexIVFPQ(codebook_size=8000, db_features=np.concatenate(feats), nb_features=nb_features)
        index.train()

        print("#INDEXBUILDING : index built and trained")
    else:
        print("Index loaded")

    start = time.time()

    ranks = []
    for j, request in enumerate(queriesdf['filename']):

        if j % 10 == 0:
            print("#IMAGESEARCH : Treated {} request images out of {}".format(j, len(df.index)))

        # Get the request descriptors
        if not os.path.exists(os.path.join(feats_dir, os.path.splitext(df.iloc[j]['filename'])[0] + '.delf')):
            print("Feature not found")
            continue
        # 0: location, 1: scale, 2: descriptor, 3: attention score
        feat = ReadFromFile(os.path.join(feats_dir, os.path.splitext(df.iloc[j]['filename'])[0] + '.delf'))
        requestlocations = feat[0]
        requestdescriptors = feat[2].astype('float32')

        #requestlocations = locations[ownership == request]

        # Get K nearest descriptors in database
        matched_descriptors, similarity = index.search(requestdescriptors)

        ranks.append(matched_descriptors)

        if useRANSAC:
            # Now for each image, try to fit a AffineTransform model using RANSAC and store the number of inliers
            # as the score for this image
            candidate_images = np.unique(corresponding_images)
            for candidate in candidate_images:

                # Select feature locations for putative matches.
                # Get locations of matched descriptors belonging to candidate image :
                # If a descriptor from request has been matched to two or more different descriptors in candidate image,
                # duplicate request descriptor as many times as needed
                flattened_correponding_images = np.reshape(corresponding_images, nb_requestdescriptors * k)
                flattened_best_matches = np.reshape(matched_descriptors, nb_requestdescriptors * k)
                candidate_locations = locations[flattened_best_matches[flattened_correponding_images == candidate]]
                # candidate_locations is a list of the locations of descriptors in image candidate that have been matched

                number_of_matches = np.sum(corresponding_images == candidate, axis=1)
                request_locations = []
                for i, count in np.ndenumerate(number_of_matches):
                    if count == 0:
                        continue
                    if count == 1:
                        request_locations.append(requestlocations[i])
                    if count > 1:
                        request_locations.append(requestlocations[i])
                        for _ in range(count - 1):
                            request_locations.append(np.array([0, 0]))

                request_locations = np.asarray(request_locations)
                # Perform geometric verification using RANSAC.
                if candidate_locations.shape[0] == 0 or request_locations.shape[0] == 0 or candidate_locations.size == 0 \
                        or request_locations.size == 0:
                    continue

                else:
                    try:
                        inliers = verify_RANSAC(candidate_locations, np.asarray(request_locations), AffineTransform)
                    except np.linalg.LinAlgError:
                        # Handling unexpected errors during RANSAC verification
                        print("Warning! LinAlgError encountered while processing image")
                        continue

                if inliers is None:
                    continue

                sumofinliers[candidate] = sum(inliers)

    ranks = np.array(ranks)

    map, aps, pr, prs = compute_map(ranks, gnd)
    #maps, aps = compute_map_per_class(ranks, gnd)
    print(map)

    end = time.time()
    print("search took {} seconds".format(end - start))

    return map


def ransac_rerank_from_global(ranks, df, queriesdf, gnd, feats_dir):
    """Compute mAP on the retrieval dataset given in the dataframe
    """

    # Keep n closest images
    n = 8

    reranked = ranks

    for j, query in enumerate(queriesdf['filename']):
        queryfeat_path = os.path.join(feats_dir, os.path.splitext(queriesdf.iloc[j]['filename'])[0] + '.delf')
        # Now for each image, try to fit a AffineTransform model using RANSAC and store the number of inliers
        # as the score for this image
        inliers = np.zeros(n)
        candidate_images = ranks[j, :n]

        for i, candidate in enumerate(candidate_images):
            candidatefeat_path = os.path.join(feats_dir, os.path.splitext(df.iloc[candidate]['filename'])[0] + '.delf')
            try:
                inliers[i] = match_two_no_plot(queryfeat_path, candidatefeat_path)
            except:
                inliers[i] = 0

        reranked[j, :n] = candidate_images[np.argsort(-inliers)]
        print(j)

    #oldmap, _, _, _ = compute_map(reranked, gnd)
    newmap, aps, pr, prs = compute_map(reranked, gnd)
    #print(oldmap)
    print(newmap)

    return newmap


def check_results(dataroot, csvpath):
    df = pd.read_csv(csvpath, index_col=0)

    for idx, row in df.iterrows():
        query = row['id']
        queryfullpath = os.path.join(dataroot, query + '.jpg')
        ref = row['ref']
        reffullpath = os.path.join(dataroot, ref + '.jpg')
        queryim = Image.open(queryfullpath)
        refim = Image.open(reffullpath)
        boundingbox = ast.literal_eval(row['bounding_box'])
        cropped_query = queryim.crop((
            boundingbox[1] * queryim.width,
            boundingbox[0] * queryim.height,
            boundingbox[3] * queryim.width,
            boundingbox[2] * queryim.height
        ))
        affine_params = np.asarray(row['params'].split())
        affine_params[3] = affine_params[3][:-1]
        affine_params[7] = affine_params[7][:-1]
        affine_params[11] = affine_params[11][:-2]
        params = np.zeros((3, 3), dtype=np.float)
        params[0, :] = affine_params[1:4]
        params[1, :] = affine_params[5:8]
        params[2, :] = affine_params[9:]
        tf = AffineTransform(params)
        transformed_ref = warp(np.asarray(refim.convert('LA')), tf)
        queryim.show()
        cropped_query.show()
        refim.show()


def evaluate_with_attributes(df, queriesdf, feats_dir, gnd):
    """Evaluates performance and assess how attributes influence it"""
    results = {}

    # # First load and check imlist and features : each image must have an associated delf file
    # feature_paths = []
    # for image in df['path']:
    #     _, idimage = os.path.split(image)
    #     idimage, _ = os.path.splitext(idimage)
    #     featurepath = os.path.join(feats_dir, idimage + ".delf")
    #     if not os.path.exists(featurepath):
    #         print(featurepath)
    #         raise ValueError
    #     feature_paths.append(featurepath)
    #
    # print("Found {} images with corresponding .delf files".format(len(feature_paths)))
    #
    # nb_images = len(df.index)
    #
    # feats = []
    # nb_features = []
    # for i, feat in enumerate(feature_paths):
    #     # 0: location, 1: scale, 2: descriptor, 3: attention score
    #     feat = ReadFromFile(feat)
    #     if feat[0].size == 0:
    #         print("No descriptors in delf feature file {}".format(df.iloc[i]['imlist']))
    #     else:
    #         npfeats = np.asarray(feat[2].astype('float32'))
    #         feats.append(npfeats)
    #         nb_features.append(npfeats.shape[0])

    #index = IndexIVFPQ(codebook_size=8000, db_features=np.concatenate(feats), nb_features=nb_features)
    #index.train()
    index = faiss.read_index("ivfpq_alegoria.index")

    ranks = []
    for j, request in enumerate(queriesdf['filename']):
        if j % 10 == 0:
            print("#IMAGESEARCH : Treated {} request images out of {}".format(j, len(df.index)))

        # Get the request descriptors
        if not os.path.exists(os.path.join(feats_dir, os.path.splitext(df.iloc[j]['filename'])[0] + '.delf')):
            print("Feature not found")
            continue
        # 0: location, 1: scale, 2: descriptor, 3: attention score
        feat = ReadFromFile(os.path.join(feats_dir, os.path.splitext(df.iloc[j]['filename'])[0] + '.delf'))
        requestdescriptors = feat[2].astype('float32')

        # Get K nearest descriptors in database
        matched_descriptors, similarity = index.search(requestdescriptors)

        ranks.append(matched_descriptors)

    ranks = np.array(ranks)
    map, aps, pr, prs = compute_map(ranks, gnd)

    for attr in ['source', 'color', 'scale', 'illumination', 'orientation', 'domain', 'occlusion', 'alterations']:
        attr_unique_values = queriesdf[attr].unique()
        for uval in attr_unique_values:
            filteredqueries = queriesdf[queriesdf[attr] == uval].index.tolist()

            # Only keep aps where query attribute == uval
            amap = np.mean(aps[filteredqueries])

            results[attr+"="+str(uval)+"_n="+str(len(filteredqueries))+" --- mAP="] = amap

    df = pd.DataFrame.from_dict(results, orient="index")
    df.to_csv("data.csv")

    return results


def recompute_ground_truth(gnd, fqueries_index):
    # Recomputes the ground truth after filtering with specific attribute values. fqueries_index is the list of indices of queries afte$
    for key in gnd:
        positives = gnd[key]['positive']
        intersection = np.intersect1d(fqueries_index, np.array(positives).astype(int)).tolist()
        ignore = np.setdiff1d(np.array(positives).astype(int), fqueries_index).tolist()
        gnd[key]['positive'] = intersection
        gnd[key]['ignore'] = ignore
    return gnd


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config_path',
        type=str,
        default='delf_config_example.pbtxt',
        help=""" Path to DelfConfig proto text file with configuration to be used for DELF extraction. """)
    parser.add_argument(
        '--queriesdf',
        type=str,
        default='queries.csv',
        help=""" Path to the queries csv. """)
    parser.add_argument(
        '--base_path',
        type=str,
        default='test_images/',
        help=""" Path to folder with base images """)
    parser.add_argument(
        '--basefeat_path',
        type=str,
        default='test_features/',
        help=""" Path to file containing DELF features for the image base. """)
    args, unparsed = parser.parse_known_args()

    queriesdf = pd.read_csv(args.queriesdf)
    queriesdf['path'] = queriesdf.apply(
        lambda x: os.path.join("/data/dgominski/alegoria_v2", "queries", x['classname'].split(',')[0], x['filename']), axis=1)
    queriesdf['positive'] = queriesdf['positive'].apply(lambda x: x.strip('[').strip(']').split(','))
    queriesdf['ignore'] = queriesdf.index.to_numpy()

    def get_immediate_subdirectories(a_dir):
        return [name for name in os.listdir(a_dir) if os.path.isdir(os.path.join(a_dir, name))]

    distractorsfolder = os.path.join("/data/dgominski/alegoria_v2", "distractors")
    print('>> Browsing distractors folder {}'.format(distractorsfolder))
    subfolders = sorted(get_immediate_subdirectories(distractorsfolder))
    distractors = []
    for folder in subfolders:
        distractors.extend(glob.glob(os.path.join(distractorsfolder, folder, "*.jpg")))

    gnd = queriesdf.to_dict(orient='index')
    distractorsdf = pd.DataFrame({'path': distractors, 'class': np.zeros(len(distractors))})
    distractorsdf['filename'] = distractorsdf['path'].apply(lambda x: os.path.split(x)[1])
    #df = queriesdf.append(distractorsdf, ignore_index=True)
    df = queriesdf

    #ranks = np.load("/data/dgominski/ranks.npy")
    #ransac_rerank_from_global(ranks, df, queriesdf, gnd, args.basefeat_path)
    evaluate_with_attributes(df, queriesdf, args.basefeat_path, gnd)
    #benchmark_on_dataframe(df, queriesdf, args.basefeat_path, gnd)

    #check_results("/data/dgominski/googleLandmarks/train", "data/results.csv")

    # # Read list of source images
    # baseimages = read_image_paths(conf.base_path)
    #
    # # Read features for the image base
    # basefeatures = ReadAllFromFile(conf.basefeat_path)
    # num_images_base = len(basefeatures)
    #
    # locations = []
    # descriptors = []
    # for i, descriptor in enumerate(basefeatures):
    #     [base_locations, _, base_descriptors, _, _, _] = descriptor
    #     locations.extend(base_locations)
    #     descriptors.extend(base_descriptors)
    # locations = np.asarray(locations)
    # descriptors = np.asarray(descriptors).astype('float32')
    #
    # with open("data/clusters_centers.json", "r") as read_file:
    #     clusters_centers = np.array(json.load(read_file))
    #
    # # Get PQ objects
    # PQ = []
    # filelist = input_fn("data/faiss_indexes", ".index")
    # for file in filelist:
    #     PQ.append(faiss.read_index(file))
    #
    # with open("data/assignment_table.json", "r") as read_file:
    #     assignment_table = np.array(json.load(read_file))
    #
    # with open("data/index_ownership.json", "r") as read_file:
    #     index_ownership = np.array(json.load(read_file))
    #
    # # Check if size is correct
    # assert len(baseimages) == num_images_base
    # assert index_ownership.shape[0] == len(locations)
    #
    # current_directory = os.getcwd()
    #
    # k = 10
    # targetdir = os.path.join(current_directory, 'resultsGrayscale' + str(k))
    # os.makedirs(targetdir)
    # for i in range(num_images_base):
    #     file = open(targetdir + '/' + baseimages[i][-11:-4] + ".txt", 'w+')
    #     results, score = find_most_similar_benchmark(locations, descriptors, index_ownership, assignment_table,
    #                                                  clusters_centers, baseimages, PQ,
    #                                                  request=i, k=k, useRANSAC=False)
    #     for result in results:
    #         file.write(baseimages[result][-11:-4] + " " + str(score[result]))
    #         file.write("\n")
    #     file.close()
    #
    # #conf = argparse.Namespace(list_image_path="/home/dimitri/Pictures/datasets/testALEGORIA", output_dir="/home/dimitri/Pictures/datasets/testALEGORIA", config_path="config/delf_config_example.pbtxt", basefeat_path="/home/dimitri/Pictures/datasets/testALEGORIA/index.delf")
