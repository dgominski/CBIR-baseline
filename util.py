from glob import glob
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from skimage.feature import plot_matches
import os
from os import walk
from os.path import isdir, exists
import tensorflow as tf
from google.protobuf import text_format
import feature_extractor
import time
import re
from feature_io import WriteToFile
from extract_boxes import _FilterBoxesByScore
from protos import delf_config_pb2

_DELF_EXT = '.delf'
_STATUS_CHECK_ITERATIONS = 5


def stringnumbers(string):
    return int(''.join(filter(str.isdigit, string)))


def input_fn(path, extensions):
    """
    Returns a list of files in path with given extensions
    :param path: directory to search
    :param extensions: single or list of extensions to consider
    :return: list of files sorted 1) by type 2) by name
    """
    list_of_images = []
    for ext in extensions:
        list_of_images.extend(glob(path+'/**/*'+ext, recursive=True))
    return sorted(list_of_images, key=stringnumbers)


def write_image_paths(list_of_images, path):
    file = open(path, "w")
    for item in list_of_images:
        file.write(item)
        file.write("\n")
    file.close()


def read_image_paths(path):
    file = open(path, "r")
    list_of_images = file.read().splitlines()
    file.close()
    return list_of_images


def getrecursivefilelist(dirName, extensions=None):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()

    if extensions and not sum(['.' in ext for ext in extensions]):
        raise Warning("getListOfFiles method - Extensions should be given with a dot")

    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getrecursivefilelist(fullPath, extensions=extensions)
        else:
            if extensions:
                filename, ext = os.path.splitext(fullPath)
                if ext in extensions:
                    allFiles.append(fullPath)
            else:
                allFiles.append(fullPath)
    return allFiles


def display_correspondences(img1_path, img2_path, locations_1, locations_2, idxs):
    _, ax = plt.subplots()
    img_1 = mpimg.imread(img1_path)
    img_2 = mpimg.imread(img2_path)
    if img_1.ndim != 3:
        img_1 = np.repeat(img_1[:, :, np.newaxis], 3, axis=2)
    if img_2.ndim != 3:
        img_2 = np.repeat(img_2[:, :, np.newaxis], 3, axis=2)
    inlier_idxs = np.nonzero(idxs)[0]
    plot_matches(
        ax,
        img_1,
        img_2,
        locations_1,
        locations_2,
        np.column_stack((inlier_idxs, inlier_idxs)),
        matches_color='b')
    ax.axis('off')
    ax.set_title('DELF correspondences')
    plt.show()


def generate_ground_truth(directory, extension):
    """
    Generates a dictionary linking each image in every subfolder of directory to a class
    :param directory: path to a directory containing multiple subfolders with images inside
    :param extension: file extension to consider
    :return: dictionary
    """
    if not isdir(directory):
        raise NotADirectoryError("This directory does not exist, aborting")

    subfolders = next(walk(directory))[1]
    book_of_truth = {}
    for sub in subfolders:
        path = directory+"/"+sub
        list_of_images = input_fn(path, extension)
        book_of_truth[sub] = [l[-11:-4] for l in list_of_images]

    return book_of_truth


def extract_features(conf, image_paths):

    tf.logging.set_verbosity(tf.logging.INFO)

    # Parse DelfConfig proto.
    config = delf_config_pb2.DelfConfig()
    with tf.gfile.FastGFile(conf.config_path, 'r') as f:
        text_format.Merge(f.read(), config)

    num_images = len(image_paths)

    data = []

    # Tell TensorFlow that the model will be built into the default Graph.
    with tf.Graph().as_default():
        # Reading list of images.
        filename_queue = tf.train.string_input_producer(image_paths, shuffle=False)
        reader = tf.WholeFileReader()
        _, value = reader.read(filename_queue)
        image_tf = tf.image.decode_jpeg(value, channels=3)

        with tf.Session() as sess:
            # Initialize variables.
            init_op = tf.global_variables_initializer()
            sess.run(init_op)

            # Loading model that will be used.
            tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING],
                                       config.model_path)
            graph = tf.get_default_graph()
            input_image = graph.get_tensor_by_name('input_image:0')
            input_score_threshold = graph.get_tensor_by_name('input_abs_thres:0')
            input_image_scales = graph.get_tensor_by_name('input_scales:0')
            input_max_feature_num = graph.get_tensor_by_name(
                'input_max_feature_num:0')
            boxes = graph.get_tensor_by_name('boxes:0')
            raw_descriptors = graph.get_tensor_by_name('features:0')
            feature_scales = graph.get_tensor_by_name('scales:0')
            attention_with_extra_dim = graph.get_tensor_by_name('scores:0')
            attention = tf.reshape(attention_with_extra_dim,
                                   [tf.shape(attention_with_extra_dim)[0]])

            locations, descriptors = feature_extractor.DelfFeaturePostProcessing(
                boxes, raw_descriptors, config)

            # Start input enqueue threads.
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)
            start = time.clock()
            for i in range(num_images):
                # Write to log-info once in a while.
                if i == 0:
                    tf.logging.info('Starting to extract DELF features from images...')
                elif i % _STATUS_CHECK_ITERATIONS == 0:
                    elapsed = (time.clock() - start)
                    tf.logging.info('Processing image %d out of %d, last %d '
                                    'images took %f seconds', i, num_images,
                                    _STATUS_CHECK_ITERATIONS, elapsed)
                    start = time.clock()

                # # Get next image.
                im = sess.run(image_tf)

                # Extract and save features.
                (locations_out, descriptors_out, feature_scales_out,
                 attention_out) = sess.run(
                    [locations, descriptors, feature_scales, attention],
                    feed_dict={
                        input_image:
                            im,
                        input_score_threshold:
                            config.delf_local_config.score_threshold,
                        input_image_scales:
                            list(config.image_scales),
                        input_max_feature_num:
                            config.delf_local_config.max_feature_num
                    })

                data.append([locations_out,
                              feature_scales_out, descriptors_out,
                              attention_out, None])
                # None at the end because orientations not available (see protobuf structure)

            # Finalize enqueue threads.
            coord.request_stop()
            coord.join(threads)

    return data


def extract_and_save(conf, path, output_path):
    tf.logging.set_verbosity(tf.logging.INFO)

    # Parse DelfConfig proto.
    config = delf_config_pb2.DelfConfig()
    with tf.gfile.FastGFile(conf.config_path, 'r') as f:
        text_format.Merge(f.read(), config)

    num_images = len(path)

    path = path[:]
    for i in range(num_images):
        if ".jpg" not in path[i]:
            path[i] = path[i] + ".jpg"

    # Tell TensorFlow that the model will be built into the default Graph.
    with tf.Graph().as_default():
        # Reading list of images.
        filename_queue = tf.train.string_input_producer(path, shuffle=False, capacity=10)
        reader = tf.WholeFileReader()
        _, value = reader.read(filename_queue)
        image_tf = tf.image.decode_jpeg(value, channels=3)
        # image_tf = tf.image.rgb_to_grayscale(image_tf)
        # image_tf = tf.tile(image_tf, [1, 1, 3])

        with tf.Session() as sess:
            # Initialize variables.
            init_op = tf.global_variables_initializer()
            sess.run(init_op)

            # object detector
            #detector_fn = detector.MakeDetector(sess, "/tmp/d2r_frcnn_20190411/")

            # Loading model that will be used.
            tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING],
                                       config.model_path)
            graph = tf.get_default_graph()
            input_image = graph.get_tensor_by_name('input_image:0')
            input_score_threshold = graph.get_tensor_by_name('input_abs_thres:0')
            input_image_scales = graph.get_tensor_by_name('input_scales:0')
            input_max_feature_num = graph.get_tensor_by_name(
                'input_max_feature_num:0')
            boxes = graph.get_tensor_by_name('boxes:0')
            raw_descriptors = graph.get_tensor_by_name('features:0')
            feature_scales = graph.get_tensor_by_name('scales:0')
            attention_with_extra_dim = graph.get_tensor_by_name('scores:0')
            attention = tf.reshape(attention_with_extra_dim,
                                   [tf.shape(attention_with_extra_dim)[0]])

            locations, descriptors = feature_extractor.DelfFeaturePostProcessing(
                boxes, raw_descriptors, config)

            # Start input enqueue threads.
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)
            start = time.clock()
            for i in range(num_images):

                output_filename = delf_naming_fn(path[i])+".delf"
                if exists(os.path.join(output_path, output_filename)):
                    print("Skipping "+output_filename+", file already exists.")
                    continue

                # Write to log-info once in a while.
                if i == 0:
                    tf.logging.info('Starting to extract DELF features from images...')
                elif i % _STATUS_CHECK_ITERATIONS == 0:
                    elapsed = (time.clock() - start)
                    tf.logging.info('Processing image %d out of %d, last %d '
                                    'images took %f seconds', i, num_images,
                                    _STATUS_CHECK_ITERATIONS, elapsed)
                    start = time.clock()

                try:
                    # # Get next image.
                    im = sess.run(image_tf)

                    # Get object bounding box
                    # (boxes_out, scores_out, class_indices_out) = detector_fn(im)
                    # (selected_boxes, selected_scores,
                    #  selected_class_indices) = _FilterBoxesByScore(boxes_out[0],
                    #                                                scores_out[0],
                    #                                                class_indices_out[0],
                    #                                                0.8)

                    #im = tf.image.crop_to_bounding_box(im, selected_boxes[0])
                    # Extract and save features.
                    (locations_out, descriptors_out, feature_scales_out,
                     attention_out) = sess.run(
                        [locations, descriptors, feature_scales, attention],
                        feed_dict={
                            input_image:
                                im,
                            input_score_threshold:
                                config.delf_local_config.score_threshold,
                            input_image_scales:
                                list(config.image_scales),
                            input_max_feature_num:
                                config.delf_local_config.max_feature_num
                        })

                    WriteToFile(os.path.join(output_path, output_filename), locations_out, feature_scales_out, descriptors_out, attention_out, None)
                    # None at the end because orientations not available (see protobuf structure)

                except tf.errors.ResourceExhaustedError:
                    continue
                    
            # Finalize enqueue threads.
            coord.request_stop()
            coord.join(threads)


def delf_naming_fn(string):
    if not "/" in string or not "." in string:
        return string
    else:
        return string.split(".")[-2].split("/")[-1]

def get_naming_function(dataroot):
    def wrapper(row):
        return os.path.join(dataroot, row['class'], row['filename'])
    return wrapper

if __name__ == "__main__":
    generate_ground_truth("/home/dimitri/Pictures/datasets/testALEGORIA", ["jpg", "jpeg"])