import os
import pickle
import csv
import glob
import numpy as np
import pandas as pd

DATASETS = ['oxford5k', 'paris6k', 'roxford5k', 'rparis6k', 'skraa', 'holidays', 'google-landmarks', 'ukbench', 'alegoria']

def configdataset(dataset, dir_main, chooseclass=None):

    dataset = dataset.lower()

    if dataset not in DATASETS:    
        raise ValueError('Unknown dataset: {}!'.format(dataset))

    if dataset == 'skraa':
        cfg = generate_GT_from_dataframe(dir_main)
    elif dataset == 'google-landmarks':
        cfg = googleLandmarksBB(dir_main, "/data/dgominski/googleLandmarks/train.csv",
                                "/data/dgominski/googleLandmarks/boundingboxlist",
                                ["/data/dgominski/googleLandmarks/boxes_split1.csv",
                                 "/data/dgominski/googleLandmarks/boxes_split2.csv"])
    elif dataset == 'ukbench':
        cfg = generate_GT_from_imagefolder(dir_main)
    elif dataset == 'alegoria':
        cfg = generate_GT_from_imagesubfolders(dir_main, chooseclass)
    else:
        # loading imlist, qimlist, and gnd, in cfg as a dict
        gnd_fname = os.path.join(dir_main, 'gnd_{}.pkl'.format(dataset))
        with open(gnd_fname, 'rb') as f:
            cfg = pickle.load(f)
        cfg['gnd_fname'] = gnd_fname

        for i, im in enumerate(cfg['imlist']):
            cfg['imlist'][i] = os.path.join(dir_main, "jpg", im)
        for j, qim in enumerate(cfg['imlist']):
            cfg['imlist'][j] = os.path.join(dir_main, "jpg", qim)

    cfg['ext'] = '.jpg'
    cfg['qext'] = '.jpg'
    cfg['dir_data'] = os.path.join(dir_main, dataset)
    cfg['dir_images'] = os.path.join(cfg['dir_data'], 'jpg')

    cfg['n'] = len(cfg['imlist'])
    cfg['nq'] = len(cfg['qimlist'])

    cfg['im_fname'] = config_imname
    cfg['qim_fname'] = config_qimname

    cfg['dataset'] = dataset

    return cfg


def config_imname(cfg, i):
    return os.path.join(cfg['dir_images'], cfg['imlist'][i] + cfg['ext'])


def config_qimname(cfg, i):
    return os.path.join(cfg['dir_images'], cfg['qimlist'][i] + cfg['qext'])


def generate_GT_from_dataframe(data_root, n_queries=5, n_images=50, overwrite=True):
    '''
    Parses the dataframe containing the information about the test dataset to generate the ground truth dictionary
    :param data_root: path to the root folder of the dataset
    :param n_queries: portion of the dataset to take as queries
    :param n_images: number of images to put in the testing set
    :param overwrite: if True, the existing ground truth dictionary is computed again
    :return: the ground_truth dictionary
    '''

    if os.path.exists(os.path.join(data_root, "gnd_test.pkl")) and not overwrite:
        print("Ground truth dictionary already existing.")
        gt = pickle.load(open(os.path.join(data_root,"gnd_test.pkl"), 'rb'))
        print("Loaded the test set of {} queries in {} images.".format(len(gt['qimlist']), len(gt['imlist'])))
        return gt

    print('>> Opening the test set DataFrame.. {}'.format(data_root+"/testDataframe.pkl"))

    data = pd.read_pickle(os.path.join(data_root, "testDataframe.pkl"))

    print("The test dataset contains {0} images in {1} classes ".format(len(data.index), data['class'].unique().size))

    print("Selecting {} random queries in a set of {} images to create the test subset".format(n_queries, n_images if n_images is not None else len(data.index)))

    if n_images is not None:
        data = data.sample(n=n_images)
        data.reset_index(inplace=True, drop=True)
    queries = data.sample(n=n_queries)

    gt = {'qidx': [],
          'gnd': [],
          'imlist': data['path'].str.split('.').str[0].tolist(),
          'qimlist': queries['path'].str.split('.').str[0].tolist()}

    for idx, query in queries.iterrows():
        # Get all images from the same class in the subset
        positives = data[data['class'] == query['class']]

        gt['qidx'].append(idx)
        gt['gnd'].append({'ok':positives.index.tolist()})

    pickle.dump(gt, open(os.path.join(data_root, "gnd_test.pkl"), 'wb'))
    return gt


def generate_GT_from_csv(data_root, csv_path=None, n_queries=None, overwrite=True):
    '''
    Parses the csv containing the information about the dataset to generate the ground truth dictionary
    :param data_root: path to the root folder of the dataset
    :param n_queries: portion of the dataset to take as queries
    :param overwrite: if True, the existing ground truth dictionary is computed again
    :return: the ground_truth dictionary
    '''

    if csv_path is not None:
        CSV = csv_path
    else:
        CSV = os.path.join(data_root, "train.csv")

    if os.path.exists(os.path.join(data_root, "gnd_test.pkl")) and not overwrite:
        print("Ground truth dictionary already existing.")
        gt = pickle.load(open(os.path.join(data_root, "gnd_test.pkl"), 'rb'))
        print("Loaded the test set of {} queries in {} images.".format(len(gt['qimlist']), len(gt['imlist'])))
        return gt

    print('>> Opening the test set csv.. {}'.format(CSV))

    images = []
    url = []
    classes = []
    with open(CSV) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for i, row in enumerate(csv_reader):
            if i==0 or row[2] is None:
                continue
            try:
                int(row[2])
            except:
                continue
            images.append(os.path.join(data_root,row[0]))
            url.append(row[1])
            classes.append(row[2])
    images = np.asarray(images, dtype=np.str)
    url = np.asarray(url, dtype=np.str)
    classes = np.asarray(classes, dtype=np.uint32)

    print("The test dataset contains {0} images in {1} classes ".format(len(images), np.unique(classes).size))

    print("Selecting {} random queries".format(n_queries))

    if n_queries is not None:
        queries = np.random.choice(images, n_queries)
    else:
        queries = images

    gt = {'qidx': [],
          'gnd': [],
          'imlist': images.tolist(),
          'qimlist': queries.tolist()}

    # for idx, query in enumerate(queries):
    #     imlist_idx = np.where(query == images)[0][0]
    #     # Get all images from the same class in the subset
    #     positives = images[classes == classes[idx]]
    #     imlist_positives_idx = []
    #     for positive in positives:
    #         imlist_positives_idx.append(np.where(images == positive)[0][0])
    #
    #     gt['qidx'].append(imlist_idx.tolist())
    #     gt['gnd'].append({'ok': imlist_positives_idx})
    #
    pickle.dump(gt, open(os.path.join(data_root, "gnd_test.pkl"), 'wb'))
    return gt

def googleLandmarksBB(csv_path, bb_path, bbcsvs, savepath):

    if os.path.exists(os.path.join(savepath, "glbbsdataframe.pkl")):
        print("Loading dataset dataframe from disk")
        df = pd.read_pickle(os.path.join(savepath, "glbbsdataframe.pkl"))
        return df

    print('>> Opening the list of bounding box images.. {}'.format(bb_path))
    bbs = pickle.load(open(bb_path, "rb"))
    bbs = np.asarray(bbs)
    ids = np.asarray([x.split('/')[-1].split('.')[-2] for x in bbs])
    df1 = pd.DataFrame(data=np.hstack((ids[:, np.newaxis], bbs[:, np.newaxis])), columns=['id', 'fullpath'])
    df1['intid'] = df1.id.apply(lambda x: int(x, 16))

    print('>> Opening the csv.. {}'.format(csv_path))
    images = []
    url = []
    classes = []
    with open(csv_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for i, row in enumerate(csv_reader):
            if i==0 or row[2] is None:
                continue
            try:
                int(row[2])
            except:
                continue
            images.append(row[0])
            url.append(row[1])
            classes.append(row[2])

    df2 = pd.DataFrame()
    df2['id'] = images
    df2['url'] = url
    df2['class'] = classes
    df2['intid'] = df2.id.apply(lambda x: int(x, 16))

    print('>> Opening the bounding box annotations..')
    bbxs = []
    for bbcsv in bbcsvs:
        with open(bbcsv, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for i, row in enumerate(reader):
                if i == 0: continue
                values = row[1].split(' ')
                values = [float(value) for value in values]
                bbxs.append((row[0], values))
    bbxs = np.asarray(bbxs)
    df3 = pd.DataFrame(data=bbxs, columns=['id', 'bounding_box'])
    df3['intid'] = df3.id.apply(lambda x: int(x, 16))

    df = df1.merge(df2, how='inner').merge(df3, how='inner')

    print("Saving dataset dataframe to disk")
    pd.to_pickle(df, os.path.join(savepath, "glbbsdataframe.pkl"))

    return df

def birds(dataroot, imagelist, savepath):

    if os.path.exists(os.path.join(savepath, "dataframe.pkl")):
        print("Loading dataset dataframe from disk")
        df = pd.read_pickle(os.path.join(savepath, "dataframe.pkl"))
        return df

    print('>> Opening the list of bounding box images.. {}'.format(imagelist))
    images = []
    file = open(imagelist, "r")
    for line in file:
        images.append(line)
    images = np.asarray(images)
    fullpath = np.asarray([os.path.join(dataroot, "images", x).rstrip() for x in images])
    ids = np.asarray([x.split('/')[-1].split('.')[-2].rstrip() for x in images])
    classes = np.asarray([x.split('/')[-1].split('_0')[-2].rstrip() for x in images])
    df = pd.DataFrame(data=np.hstack((ids[:, np.newaxis], fullpath[:, np.newaxis], classes[:, np.newaxis])), columns=['id', 'fullpath', 'class'])
    df = df[df.id != "xxx"]

    pd.to_pickle(df, os.path.join(savepath, "dataframe.pkl"))

    return df

def generate_GT_from_imagefolder(data_root):
    '''
    suited for UKBench dataset
    Parses a folder, generates the ground truth based on file names. Each class contains 4 samples.
    :param data_root: path to the root folder of the dataset
    :return: the ground_truth dictionary
    '''

    if os.path.exists(os.path.join(data_root, "gnd_test.pkl")):
        print("Ground truth dictionary already existing.")
        gt = pickle.load(open(os.path.join(data_root,"gnd_test.pkl"), 'rb'))
        print("Loaded the test set of {} queries in {} images.".format(len(gt['qimlist']), len(gt['imlist'])))
        return gt

    print('>> Browsing the folder'.format(data_root+"/testDataframe.pkl"))

    images = sorted(glob.glob(os.path.join(data_root, "*.jpg")))
    images = [image.split(".")[-2] for image in images]
    nb_classes = len(images) // 4
    classes = []
    for i in range(nb_classes): classes.extend([i, i, i, i])

    images = np.asarray(images, dtype=np.str)
    classes = np.asarray(classes, dtype=np.uint32)

    print("The test dataset contains {0} images in {1} classes ".format(len(images), np.unique(classes).size))

    print("Selecting the first image of each class as the input query")

    queries = images[0::4]

    gt = {'qidx': [],
          'gnd': [],
          'imlist': images.tolist(),
          'qimlist': queries.tolist()}

    for idx, query in enumerate(queries):
        imlist_idx = np.where(query == images)[0][0]
        # Get all images from the same class in the subset
        positives = images[classes == classes[imlist_idx]]
        imlist_positives_idx = []
        for positive in positives:
            imlist_positives_idx.append(np.where(images == positive)[0][0])

        gt['qidx'].append(imlist_idx.tolist())
        gt['gnd'].append({'ok': imlist_positives_idx})

    pickle.dump(gt, open(os.path.join(data_root, "gnd_test.pkl"), 'wb'))
    return gt


def generate_GT_from_imagesubfolders(data_root, chooseclass):
    '''
        suited for ALEGORIA dataset : n folders containing a different number of images each, each folder is one class.
        Images located in any folder that is not named with a number are considered distractors
        :param data_root: path to the root folder of the dataset
        :return: the ground_truth dictionary
        '''

    # if os.path.exists(os.path.join(data_root, "gnd_test.pkl")):
    #     print("Ground truth dictionary already existing.")
    #     gt = pickle.load(open(os.path.join(data_root, "gnd_test.pkl"), 'rb'))
    #     print("Loaded the test set of {} queries in {} images.".format(len(gt['qimlist']), len(gt['imlist'])))
    #     return gt

    print('>> Browsing folder {}'.format(data_root))

    subfolders = sorted(get_immediate_subdirectories(data_root))

    classes = []
    images = []
    distractors = []
    nb_images_per_class = []
    for folder in subfolders:
        if isNumbers(folder):
            classes.append(folder)
            current_directory = os.path.join(data_root, folder)
            current_images = glob.glob(os.path.join(current_directory, "*.jpg"))
            images.extend(current_images)
            nb_images_per_class.append(len(current_images))
        else:
            distractors.extend(glob.glob(os.path.join(data_root, folder, "*.jpg")))

    classes = np.asarray(classes, dtype=np.int32)
    classes = np.repeat(classes, nb_images_per_class)

    distractors = [distractor.split(".")[-2] for distractor in distractors]
    images = [image.split(".")[-2] for image in images]
    if chooseclass is not None:
        queries = np.asarray(images)[classes == chooseclass].tolist()
    else:
        queries = images.copy()
    images.extend(distractors)
    images = np.asarray(images, dtype=np.str)

    print("The test dataset contains {0} images in {1} classes ".format(len(images), np.unique(classes).size))

    print("Selecting the first image of each class as the input query")

    gt = {'qidx': [],
          'gnd': [],
          'imlist': images.tolist(),
          'qimlist': queries,
          'classes': classes}

    classes = np.concatenate((classes, len(distractors) * [-1]))

    for idx, query in enumerate(queries):
        imlist_idx = np.where(query == images)[0][0]
        # Get all images from the same class in the subset
        positives = images[classes == classes[imlist_idx]]
        imlist_positives_idx = []
        for positive in positives:
            imlist_positives_idx.append(np.where(images == positive)[0][0])

        gt['qidx'].append(imlist_idx.tolist())
        gt['gnd'].append({'ok': imlist_positives_idx})

    # pickle.dump(gt, open(os.path.join(data_root, "gnd_test.pkl"), 'wb'))
    return gt


def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]


def isNumbers(inputString):
    "Returns True if the input string is only numbers"
    return all(char.isdigit() for char in inputString)