import faiss
import os
import numpy as np
np.random.seed(0)
from abc import abstractmethod


_NORM_SQUARED_TOLERANCE = 1e-12
ESPILON = 1e-6

class Index:
    """
    Index object for storing a database of local descriptors and performing nearest neighbour search.
    The index is trained on a (N,D) database of features.
    Train step builds the visual word dictionary and defines
        self.index_ownership ((N) array) linking each feature to its source image
        self.index_assignment ((N) array) linking each feature to its assigned visual word
        self.size (int) number of images in the database
    Search step takes as input a (M,D) array of features
    and returns a ((self.size) array) giving the similarity score to the database images.
            Params
    k: number of nearest neighbours to consider when matching a node with database nodes.
            Methods
    train: performs KMeans clustering and quantization to build an inverted index.
    save: saves the index to disk
    load: loads the index from disk
    search: gets closest neighbours to input list of descriptors
    """
    def __init__(self, codebook_size, db_features, nb_features):
        """
                :param db_features (ndarray/list): NxD array or list of database features
                :param nb_features (ndarray/list): N elements array or list giving the number of features per database image
        """
        self.codebook_size = codebook_size
        self.size = len(nb_features) # Number of images in the base
        self.db_features = db_features
        self.index_ownership = np.repeat(np.arange(0, len(nb_features)), nb_features)

    @abstractmethod
    def train(self):
        pass

    @abstractmethod
    def search(self, query_features):
        """
        Searches the database to get closest images
        :param query_features (ndarray/list): MxD array or list of query node embeddings (features)
        :return
        """
        pass

    def save(self, path):
        """
        Saves index and ownership array to disk
        :param path: destination folder
        :return
        """
        faiss.write_index(self.index, os.path.join(path, "ivfpq.index"))
        np.save(os.path.join(path, "ownership.npy"))

    def load(self, path):
        """
        Loads index and ownership array from disk
        :param path: source folder
        :return:
        """
        self.index = faiss.read_index(os.path.join(path, "ivfpq.index"))
        self.index_ownership = np.load(os.path.join(path, "ownership.npy"))

    def cluster(self, database):
        """
        Performs Kmeans clustering to define visual words.
        :param database: (NxD) array of local features
        :return:
        """
        ncentroids = self.codebook_size
        niter = 20
        verbose = True
        d = database.shape[1]
        self.kmeans = faiss.Kmeans(d, ncentroids, niter=niter, verbose=verbose)
        self.kmeans.train(database)
        self.visual_words = self.kmeans.centroids

    def assign(self, features):
        """
        Assigns given features to visual words
        :param features: (NxD) array of local features
        :return: assigned visual words indexes
        """
        distances, assigned_clusters = self.kmeans.index.search(features, 1)
        return assigned_clusters.squeeze()

    @abstractmethod
    def measure_similarity(self, query_features):
        pass

    def compute_idf(self):
        self.idf = np.zeros(self.visual_words.shape[0])
        for c in range(self.visual_words.shape[0]):
            self.idf[c] = np.log(self.size / (np.unique(self.index_ownership[self.index_assignment==c]).shape[0] + 1)) + 1

    @staticmethod
    def selectivity_function(x, threshold=0.1, alpha=3.0):
        x = np.where(x>threshold, x, 0)
        return np.sign(x) * np.power(np.abs(x), alpha)


class IndexVLAD(Index):
    def __init__(self, codebook_size, db_features, nb_features):
        Index.__init__(self, codebook_size, db_features, nb_features)

    def train(self):
        """
        :param db_features (ndarray/list): NxD array or list of database features
        :param ownership (ndarray/list): N elements array or list linking each feature to its source image ID
        :return:
        """
        index_features = np.array(self.db_features).astype('float32')

        self.cluster(index_features)

        self.size = np.max(self.index_ownership) + 1
        self.index_assignment = self.assign(index_features).squeeze()

        self.compute_idf()

        self.index_normalization = np.zeros(self.size)
        self.index_aggregated_features = []
        self.index_aggregated_assignment = []
        self.index_aggregated_ownership = []
        for db_image in range(self.size):
            self.index_normalization[db_image] = self.compute_normalization_factor(index_features[self.index_ownership == db_image])
            aggregated, clusters = self.aggregate(index_features[self.index_ownership == db_image])
            self.index_aggregated_features.append(np.atleast_2d(aggregated.squeeze()))
            self.index_aggregated_assignment.append(clusters)
            self.index_aggregated_ownership.append(aggregated.shape[0]*[db_image])
        self.index_aggregated_ownership = np.concatenate(self.index_aggregated_ownership)
        self.index_aggregated_features = np.concatenate(self.index_aggregated_features)
        self.index_aggregated_assignment = np.concatenate(self.index_aggregated_assignment)

    def search(self, query_features):
        """
        Searches the database to get closest images
        :param query_features (ndarray/list): MxD array or list of query node embeddings (features)
        :return
        """
        query_features = np.array(query_features).astype('float32')
        query_normalization_factor = self.compute_normalization_factor(query_features)
        similarity = query_normalization_factor * self.index_normalization * self.measure_similarity(query_features)
        ranks = np.argsort(similarity)[::-1]
        return ranks, similarity[ranks]

    def measure_similarity(self, query_features):
        query_aggregated_features, query_assigned_clusters = self.aggregate(query_features)
        similarity = np.zeros(self.size)

        for i, c in enumerate(query_assigned_clusters):
            same_cluster_features = self.index_aggregated_features[self.index_aggregated_assignment == c]
            corresponding_images = self.index_aggregated_ownership[self.index_aggregated_assignment == c]
            dot_similarity = np.dot(query_aggregated_features[i], same_cluster_features.T)
            similarity[corresponding_images] += dot_similarity

        return similarity * self.index_normalization

    def compute_normalization_factor(self, features):
        assigned_clusters = self.assign(features)
        unique_clusters = np.unique(assigned_clusters)
        return 1/np.sqrt(np.sum(self.idf[unique_clusters]))

    def aggregate(self, features):
        """
        Aggregates descriptors for a single image across visual words
        :param features: features to aggregate, must be from single image
        :return: (K x D) matrix of aggregated descriptors per image, K <= codebook_size
        """
        assignment_list = self.assign(features)
        aggregated_descs = []
        aggregated_clusters = []
        # Loop over clusters
        for i in range(self.codebook_size):
            to_select = assignment_list == i
            # if there is at least one descriptor in that cluster
            if np.sum(to_select) > 0:
                corresponding_features = features[to_select]
                # add the differences
                aggregated_descs.append(np.sum(corresponding_features - self.kmeans.centroids[i], axis=0))
                aggregated_clusters.append(i)

        return np.array(aggregated_descs), np.array(aggregated_clusters)


class IndexBoW(Index):
    def __init__(self, codebook_size, db_features, nb_features):
        Index.__init__(self, codebook_size, db_features, nb_features)

    def train(self):
        index_features = np.array(self.db_features).astype('float32')

        self.cluster(index_features)

        self.size = np.max(self.index_ownership) + 1
        self.index_assignment = self.assign(index_features).squeeze()

        self.compute_idf()

        self.index_normalization = np.zeros(self.size)
        for db_image in range(self.size):
            self.index_normalization[db_image] = self.compute_normalization_factor(index_features[self.index_ownership == db_image])

    def search(self, query_features):
        """
        Searches the database to get closest images
        :param query_features (ndarray/list): MxD array or list of query node embeddings (features)
        :return
        """
        query_features = np.array(query_features).astype('float32')
        query_normalization_factor = self.compute_normalization_factor(query_features)
        similarity = query_normalization_factor * self.measure_similarity(query_features)
        ranks = np.argsort(similarity)[::-1]
        return ranks, similarity[ranks]

    def measure_similarity(self, query_features):
        query_clusters = self.assign(query_features)
        similarity = np.zeros(self.size)
        for c in np.unique(query_clusters):
            # Browse clusters and accumulate similarity with base images with the match kernel
            n_xc = np.sum(query_clusters == c)  # Number of local features assigned to this cluster in the query
            corresponding_images = self.index_ownership[self.index_assignment == c]
            unique, counts = np.unique(corresponding_images, return_counts=True)
            similarity[unique] += self.idf[c] * n_xc * counts
        similarity *= self.index_normalization
        return similarity

    def compute_normalization_factor(self, features):
        clusters = self.assign(features)
        s = 0
        for c in np.unique(clusters):
            # Browse clusters and accumulate squared number of assigned features
            n_xc = np.sum(clusters == c)  # Number of local features assigned to this cluster
            s += self.idf[c] * n_xc**2
        return np.power(s, -0.5)


class IndexSMK(Index):
    def __init__(self, codebook_size, db_features, nb_features):
        Index.__init__(self, codebook_size, db_features, nb_features)

    def train(self):
        index_features = np.array(self.db_features).astype('float32')

        self.cluster(index_features)

        self.size = np.max(self.index_ownership) + 1
        self.index_assignment = self.assign(index_features).squeeze()

        self.compute_idf()

        self.index_normalization = np.zeros(self.size)
        self.index_residuals = index_features - self.visual_words[self.index_assignment]
        l2_norm = np.sqrt(np.sum(np.power(self.index_residuals, 2), axis=1))
        self.index_residuals = self.index_residuals * 1/np.repeat(l2_norm[:, np.newaxis], repeats=index_features.shape[1], axis=1)
        for db_image in range(self.size):
            self.index_normalization[db_image] = self.compute_normalization_factor(
                index_features[self.index_ownership == db_image])

    def search(self, query_features):
        """
        Searches the database to get closest images
        :param query_features (ndarray/list): MxD array or list of query node embeddings (features)
        :return
        """
        query_features = np.array(query_features).astype('float32')
        query_normalization_factor = self.compute_normalization_factor(query_features)
        similarity = query_normalization_factor * self.measure_similarity(query_features)
        ranks = np.argsort(similarity)[::-1]
        return ranks, similarity[ranks]

    def measure_similarity(self, query_features):
        query_clusters = self.assign(query_features).squeeze()
        similarity = np.zeros(self.size)
        for c in np.unique(query_clusters):
            # Browse clusters and accumulate similarity with base images with the match kernel
            query_residuals = query_features[query_clusters == c] - self.visual_words[c]
            l2_norm = np.sqrt(np.sum(np.power(query_residuals, 2), axis=1))
            query_residuals /= np.repeat(l2_norm[:, np.newaxis], repeats=query_features.shape[1], axis=1)

            # Get database residuals of this cluster
            db_residuals = self.index_residuals[self.index_assignment == c]
            db_corresponding_images = self.index_ownership[self.index_assignment == c]

            # Compute match kernel output
            res = np.sum(self.selectivity_function(np.matmul(query_residuals, db_residuals.T)), axis=0)
            similarity[db_corresponding_images] += self.idf[c]*res

        similarity *= self.index_normalization
        return similarity

    def compute_normalization_factor(self, features):
        clusters = self.assign(features).squeeze()
        s = 0
        for c in np.unique(clusters):
            # Browse clusters and accumulate similarity with base images with the match kernel
            query_residuals = features[clusters == c] - self.visual_words[c]
            l2_norm = np.sqrt(np.sum(np.power(query_residuals, 2), axis=1))
            query_residuals /= np.repeat(l2_norm[:, np.newaxis], repeats=features.shape[1], axis=1)

            #  Compute match kernel output
            res = np.sum(self.selectivity_function(np.matmul(query_residuals, query_residuals.T)).squeeze())
            s += self.idf[c]*res

        return np.power(s, 0.5)


class IndexASMK(Index):
    def __init__(self, codebook_size, db_features, nb_features):
        Index.__init__(self, codebook_size, db_features, nb_features)

    def train(self):
        index_features = np.array(self.db_features).astype('float32')

        self.cluster(index_features)

        self.size = np.max(self.index_ownership) + 1
        self.index_assignment = self.assign(index_features).squeeze()

        self.compute_idf()

        self.index_normalization = np.zeros(self.size)
        self.index_aggregated_features = []
        self.index_aggregated_assignment = []
        self.index_aggregated_ownership = []
        for db_image in range(self.size):
            self.index_normalization[db_image] = self.compute_normalization_factor(index_features[self.index_ownership == db_image])
            aggregated, clusters = self.aggregate(index_features[self.index_ownership == db_image])
            self.index_aggregated_features.append(np.atleast_2d(aggregated.squeeze()))
            self.index_aggregated_assignment.append(clusters)
            self.index_aggregated_ownership.append(aggregated.shape[0]*[db_image])
        self.index_aggregated_ownership = np.concatenate(self.index_aggregated_ownership)
        self.index_aggregated_features = np.concatenate(self.index_aggregated_features)
        self.index_aggregated_assignment = np.concatenate(self.index_aggregated_assignment)

    def search(self, query_features):
        """
        Searches the database to get closest images
        :param query_features (ndarray/list): MxD array or list of query node embeddings (features)
        :return
        """
        query_features = np.array(query_features).astype('float32')
        query_normalization_factor = self.compute_normalization_factor(query_features)
        similarity = query_normalization_factor * self.measure_similarity(query_features)
        ranks = np.argsort(similarity)[::-1]
        return ranks, similarity[ranks]

    def measure_similarity(self, query_features):
        query_aggregated_features, query_assigned_clusters = self.aggregate(query_features)
        similarity = np.zeros(self.size)

        for i, c in enumerate(query_assigned_clusters):
            same_cluster_features = self.index_aggregated_features[self.index_aggregated_assignment == c]
            corresponding_images = self.index_aggregated_ownership[self.index_aggregated_assignment == c]
            dot_similarity = np.dot(query_aggregated_features[i], same_cluster_features.T)
            similarity[corresponding_images] += self.selectivity_function(dot_similarity).squeeze()

        return similarity * self.index_normalization

    def compute_normalization_factor(self, features):
        clusters = np.atleast_1d(self.assign(features))
        s = 0
        for c in np.unique(clusters):
            # Browse clusters and accumulate similarity with base images with the match kernel
            query_residuals = features[clusters == c] - self.visual_words[c]
            l2_norm = np.sqrt(np.sum(np.power(query_residuals, 2), axis=1))
            query_residuals /= np.repeat(l2_norm[:, np.newaxis] + ESPILON, repeats=features.shape[1], axis=1)

            # Compute match kernel output
            res = np.sum(self.selectivity_function(np.matmul(query_residuals, query_residuals.T)).squeeze())
            s += self.idf[c]*res

        return np.power(s, -0.5)

    def aggregate(self, features):
        """
        Aggregates descriptors for a single image across visual words
        :param features: features to aggregate, must be from single image
        :return: (K x D) matrix of aggregated descriptors per image, K <= codebook_size
        """
        assignment_list = self.assign(features)
        aggregated_descs = []
        aggregated_clusters = []
        # Loop over clusters
        for i in range(self.codebook_size):
            to_select = assignment_list == i
            # if there is at least one descriptor in that cluster
            if np.sum(to_select) > 0:
                corresponding_features = features[to_select]
                # add the differences
                aggregated_desc = np.sum(corresponding_features - self.kmeans.centroids[i], axis=0)
                aggregated_descs.append(aggregated_desc / (np.sqrt(np.sum(np.power(aggregated_desc, 2))) + ESPILON)) # L2 normalization
                aggregated_clusters.append(i)

        return np.array(aggregated_descs), np.array(aggregated_clusters)


class IndexASMKv2(Index):
    """
    Class for computing similarity of aggregated local feature representations.
    Args:
      aggregation_config: AggregationConfig object defining type of aggregation to
        use.
    Raises:
      ValueError: If aggregation type is invalid.
    """

    def __init__(self, codebook_size, db_features, nb_features, type='asmk'):
        Index.__init__(self, codebook_size, db_features, nb_features)
        self._feature_dimensionality = db_features.shape[1]
        self._aggregation_type = type

        # Only relevant if using ASMK/ASMK*. Otherwise, ignored.
        self._use_l2_normalization = True
        self._alpha = 3.0
        self._tau = 0.0
        self.batch_size = 1024

        # Only relevant if using ASMK*. Otherwise, ignored.
        self._number_bits = np.array([bin(n).count('1') for n in range(256)])

    def train(self):
        index_features = np.array(self.db_features).astype('float32')

        self.cluster(index_features)

        self.size = np.max(self.index_ownership) + 1
        self.index_assignment = self.assign(index_features).squeeze()

        self.index_normalization = np.zeros(self.size)
        self.index_aggregated_features = []
        self.index_aggregated_assignment = []
        self.index_aggregated_ownership = []
        for db_image in range(self.size):
            aggregated, clusters = self._ComputeAsmk(index_features[self.index_ownership == db_image], self.visual_words)
            self.index_aggregated_features.append(np.atleast_2d(aggregated.squeeze()))
            self.index_aggregated_assignment.append(clusters)
            self.index_aggregated_ownership.append(aggregated.shape[0] * [db_image])
        self.index_aggregated_ownership = np.concatenate(self.index_aggregated_ownership)
        self.index_aggregated_features = np.concatenate(self.index_aggregated_features)
        self.index_aggregated_assignment = np.concatenate(self.index_aggregated_assignment)

    def search(self, query_features):
        """
        Searches the database to get closest images
        :param query_features (ndarray/list): MxD array or list of query features
        :return
        """
        query_features = np.array(query_features).astype('float32')
        query_aggregated_descs, query_visual_words = self._ComputeAsmk(query_features, codebook=self.visual_words)
        similarities = []
        for i, c in enumerate(query_visual_words):
            same_cluster_features = self.index_aggregated_features[self.index_aggregated_assignment == c]
            corresponding_images = self.index_aggregated_ownership[self.index_aggregated_assignment == c]
            dot_similarity = np.dot(query_aggregated_descs[i], same_cluster_features.T)
            similarities[corresponding_images] += self.selectivity_function(dot_similarity)

        similarities = np.array(similarities)
        ranks = np.argsort(similarities)[::-1]
        return ranks, similarities[ranks]

    def ComputeSimilarity(self,
                          aggregated_descriptors_1,
                          aggregated_descriptors_2,
                          feature_visual_words_1=None,
                          feature_visual_words_2=None):
        """Computes similarity between aggregated descriptors.
        Args:
          aggregated_descriptors_1: 1-D NumPy array.
          aggregated_descriptors_2: 1-D NumPy array.
          feature_visual_words_1: Used only for ASMK/ASMK* aggregation type. 1-D
            sorted NumPy integer array denoting visual words corresponding to
            `aggregated_descriptors_1`.
          feature_visual_words_2: Used only for ASMK/ASMK* aggregation type. 1-D
            sorted NumPy integer array denoting visual words corresponding to
            `aggregated_descriptors_2`.
        Returns:
          similarity: Float. The larger, the more similar.
        Raises:
          ValueError: If aggregation type is invalid.
        """
        if self._aggregation_type == 'vlad':
            similarity = np.dot(aggregated_descriptors_1, aggregated_descriptors_2)
        elif self._aggregation_type == 'asmk':
            similarity = self._AsmkSimilarity(
                aggregated_descriptors_1,
                aggregated_descriptors_2,
                feature_visual_words_1,
                feature_visual_words_2,
                binarized=False)
        elif self._aggregation_type == 'asmk*':
            similarity = self._AsmkSimilarity(
                aggregated_descriptors_1,
                aggregated_descriptors_2,
                feature_visual_words_1,
                feature_visual_words_2,
                binarized=True)
        else:
            raise ValueError('Invalid aggregation type: %d' % self._aggregation_type)

        return similarity

    def _CheckAsmkDimensionality(self, aggregated_descriptors, num_visual_words,
                                 descriptor_name):
        """Checks that ASMK dimensionality is as expected.
        Args:
          aggregated_descriptors: 1-D NumPy array.
          num_visual_words: Integer.
          descriptor_name: String.
        Raises:
          ValueError: If descriptor dimensionality is incorrect.
        """
        if len(aggregated_descriptors
               ) / num_visual_words != self._feature_dimensionality:
            raise ValueError(
                'Feature dimensionality for aggregated descriptor %s is invalid: %d;'
                ' expected %d.' % (descriptor_name, len(aggregated_descriptors) /
                                   num_visual_words, self._feature_dimensionality))

    def _SigmaFn(self, x):
        """Selectivity ASMK/ASMK* similarity function.
        Args:
          x: Scalar or 1-D NumPy array.
        Returns:
          result: Same type as input, with output of selectivity function.
        """
        if np.isscalar(x):
            if x > self._tau:
                result = np.sign(x) * np.power(np.absolute(x), self._alpha)
            else:
                result = 0.0
        else:
            result = np.zeros_like(x)
            above_tau = np.nonzero(x > self._tau)
            result[above_tau] = np.sign(x[above_tau]) * np.power(
                np.absolute(x[above_tau]), self._alpha)
        return result

    def _BinaryNormalizedInnerProduct(self, descriptors_1, descriptors_2):

        """Computes normalized binary inner product.
        Args:
          descriptors_1: 1-D NumPy integer array.
          descriptors_2: 1-D NumPy integer array.
        Returns:
          inner_product: Float.
        Raises:
          ValueError: If the dimensionality of descriptors is different.
        """
        num_descriptors = len(descriptors_1)
        if num_descriptors != len(descriptors_2):
            raise ValueError(
                'Descriptors have incompatible dimensionality: %d vs %d' %
                (len(descriptors_1), len(descriptors_2)))

        h = 0
        for i in range(num_descriptors):
            h += self._number_bits[np.bitwise_xor(descriptors_1[i], descriptors_2[i])]

        # If local feature dimensionality is lower than 8, then use that to compute
        # proper binarized inner product.
        bits_per_descriptor = min(self._feature_dimensionality, 8)

        total_num_bits = bits_per_descriptor * num_descriptors

        return 1.0 - 2.0 * h / total_num_bits

    def _AsmkSimilarity(self,
                        aggregated_descriptors_1,
                        aggregated_descriptors_2,
                        visual_words_1,
                        visual_words_2,
                        binarized=False):

        """Compute ASMK-based similarity.
        If `aggregated_descriptors_1` or `aggregated_descriptors_2` is empty, we
        return a similarity of -1.0.
        If binarized is True, `aggregated_descriptors_1` and
        `aggregated_descriptors_2` must be of type uint8.
        Args:
          aggregated_descriptors_1: 1-D NumPy array.
          aggregated_descriptors_2: 1-D NumPy array.
          visual_words_1: 1-D sorted NumPy integer array denoting visual words
            corresponding to `aggregated_descriptors_1`.
          visual_words_2: 1-D sorted NumPy integer array denoting visual words
            corresponding to `aggregated_descriptors_2`.
          binarized: If True, compute ASMK* similarity.
        Returns:
          similarity: Float. The larger, the more similar.
        Raises:
          ValueError: If input descriptor dimensionality is inconsistent, or if
            descriptor type is unsupported.
        """
        num_visual_words_1 = len(visual_words_1)
        num_visual_words_2 = len(visual_words_2)

        if not num_visual_words_1 or not num_visual_words_2:
            return -1.0

        # Parse dimensionality used per visual word. They must be the same for both
        # aggregated descriptors. If using ASMK, they also must be equal to
        # self._feature_dimensionality.
        if binarized:
            if aggregated_descriptors_1.dtype != 'uint8':
                raise ValueError('Incorrect input descriptor type: %s' %
                                 aggregated_descriptors_1.dtype)
            if aggregated_descriptors_2.dtype != 'uint8':
                raise ValueError('Incorrect input descriptor type: %s' %
                                 aggregated_descriptors_2.dtype)

            per_visual_word_dimensionality = int(
                len(aggregated_descriptors_1) / num_visual_words_1)
            if len(aggregated_descriptors_2
                   ) / num_visual_words_2 != per_visual_word_dimensionality:
                raise ValueError('ASMK* dimensionality is inconsistent.')
        else:
            per_visual_word_dimensionality = self._feature_dimensionality
            self._CheckAsmkDimensionality(aggregated_descriptors_1,
                                          num_visual_words_1, '1')
            self._CheckAsmkDimensionality(aggregated_descriptors_2,
                                          num_visual_words_2, '2')

        aggregated_descriptors_1_reshape = np.reshape(
            aggregated_descriptors_1,
            [num_visual_words_1, per_visual_word_dimensionality])
        aggregated_descriptors_2_reshape = np.reshape(
            aggregated_descriptors_2,
            [num_visual_words_2, per_visual_word_dimensionality])

        # Loop over visual words, compute similarity.
        unnormalized_similarity = 0.0
        ind_1 = 0
        ind_2 = 0
        while ind_1 < num_visual_words_1 and ind_2 < num_visual_words_2:
            if visual_words_1[ind_1] == visual_words_2[ind_2]:
                if binarized:
                    inner_product = self._BinaryNormalizedInnerProduct(
                        aggregated_descriptors_1_reshape[ind_1],
                        aggregated_descriptors_2_reshape[ind_2])
                else:
                    inner_product = np.dot(aggregated_descriptors_1_reshape[ind_1],
                                           aggregated_descriptors_2_reshape[ind_2])
                unnormalized_similarity += self._SigmaFn(inner_product)
                ind_1 += 1
                ind_2 += 1
            elif visual_words_1[ind_1] > visual_words_2[ind_2]:
                ind_2 += 1
            else:
                ind_1 += 1

        final_similarity = unnormalized_similarity
        if self._use_l2_normalization:
            final_similarity /= np.sqrt(num_visual_words_1 * num_visual_words_2)

        return final_similarity

    def _PerCentroidNormalization(self, unnormalized_vector):
        """Perform per-centroid normalization.
        Args:
          unnormalized_vector: [KxD] float tensor.
        Returns:
          per_centroid_normalized_vector: [KxD] float tensor, with normalized
            aggregated residuals. Some residuals may be all-zero.
          visual_words: Int tensor containing indices of visual words which are
            present for the set of features.
        """
        unnormalized_vector = np.reshape(
            unnormalized_vector,
            [self.codebook_size, self._feature_dimensionality])
        per_centroid_norms = np.linalg.norm(unnormalized_vector, axis=1)

        visual_words = np.reshape(
            np.where(
                per_centroid_norms > np.sqrt(_NORM_SQUARED_TOLERANCE)),
            [-1])

        l2 = np.atleast_1d(np.linalg.norm(unnormalized_vector, 2, 1))
        l2[l2 == 0] = 1
        per_centroid_normalized_vector = unnormalized_vector / np.expand_dims(l2, 1)

        return per_centroid_normalized_vector, visual_words

    def _ComputeAsmk(self, features, codebook, num_assignments=1):
        """Compute ASMK representation.
        Args:
          features: [N, D] float tensor.
          codebook: [K, D] float tensor.
          num_assignments: Number of visual words to assign a feature to.
        Returns:
          normalized_residuals: 1-dimensional float tensor with concatenated
            residuals which are non-zero. Note that the dimensionality is
            input-dependent.
          visual_words: 1-dimensional int tensor of sorted visual word ids.
            Dimensionality is shape(normalized_residuals)[0] / D.
        """
        unnormalized_vlad = self._ComputeVlad(
            features,
            codebook,
            use_l2_normalization=False,
            num_assignments=num_assignments)

        per_centroid_normalized_vlad, visual_words = self._PerCentroidNormalization(unnormalized_vlad)

        normalized_residuals = per_centroid_normalized_vlad[visual_words]
        normalized_residuals = normalized_residuals.reshape([visual_words.shape[0] * self._feature_dimensionality])

        return normalized_residuals, visual_words

    def _ComputeVlad(self,
                     features,
                     codebook,
                     use_l2_normalization=True,
                     num_assignments=1):
        """Compute VLAD representation.
        Args:
          features: [N, D] float tensor.
          codebook: [K, D] float tensor.
          use_l2_normalization: If False, does not L2-normalize after aggregation.
          num_assignments: Number of visual words to assign a feature to.
        Returns:
          vlad: [K*D] float tensor.
        """

        def _ComputeVladEmptyFeatures():
            """Computes VLAD if `features` is empty.
            Returns:
              [K*D] all-zeros tensor.
            """
            return np.zeros([self.codebook_size * self._feature_dimensionality],
                            dtype=np.float32)

        def _ComputeVladNonEmptyFeatures():
            """Computes VLAD if `features` is not empty.
            Returns:
              [K*D] tensor with VLAD descriptor.
            """
            num_features = features.shape[0]

            # Find nearest visual words for each feature. Possibly batch the local
            # features to avoid OOM.
            if self.batch_size <= 0:
                actual_batch_size = num_features
            else:
                actual_batch_size = self.batch_size

            def _BatchNearestVisualWords(ind, selected_visual_words):
                """Compute nearest neighbor visual words for a batch of features.
                Args:
                  ind: Integer index denoting feature.
                  selected_visual_words: Partial set of visual words.
                Returns:
                  output_ind: Next index.
                  output_selected_visual_words: Updated set of visual words, including
                    the visual words for the new batch.
                """
                # Handle case of last batch, where there may be fewer than
                # `actual_batch_size` features.
                batch_size_to_use = np.where(
                    ind + actual_batch_size > num_features,
                    num_features - ind,
                    actual_batch_size)

                # Denote B = batch_size_to_use.
                # K*B x D.
                tiled_features = np.reshape(
                    np.tile(
                        features[ind:ind+batch_size_to_use, 0:self._feature_dimensionality],
                        [1, self.codebook_size]), [-1, self._feature_dimensionality])
                # K*B x D.
                tiled_codebook = np.reshape(
                    np.tile(np.reshape(codebook, [1, -1]), [batch_size_to_use, 1]),
                    [-1, self._feature_dimensionality])
                # B x K.
                squared_distances = np.reshape(
                        np.square(tiled_features - tiled_codebook).mean(axis=1),
                        [batch_size_to_use, self.codebook_size])
                # B x K.
                nearest_visual_words = np.argsort(squared_distances)
                # B x num_assignments.
                batch_selected_visual_words = nearest_visual_words[0:batch_size_to_use, 0:num_assignments]
                selected_visual_words = np.concatenate(
                    [selected_visual_words, batch_selected_visual_words], axis=0)

                return ind + batch_size_to_use, selected_visual_words

            ind_batch = 0
            keep_going = lambda j, selected_visual_words: j < num_features
            selected_visual_words = np.zeros([0, num_assignments], dtype=np.int32)
            while keep_going(ind_batch, selected_visual_words):
                ind_batch, selected_visual_words = _BatchNearestVisualWords(ind_batch, selected_visual_words)
                ind_batch += 1

            # Helper function to collect residuals for relevant visual words.
            def _ConstructVladFromAssignments(ind, vlad):
                """Add contributions of a feature to a VLAD descriptor.
                Args:
                  ind: Integer index denoting feature.
                  vlad: Partial VLAD descriptor.
                Returns:
                  output_ind: Next index (ie, ind+1).
                  output_vlad: VLAD descriptor updated to take into account contribution
                    from ind-th feature.
                """
                diff = np.tile(
                    np.expand_dims(features[ind],
                                   axis=0), [num_assignments, 1]) - np.take(
                    codebook, selected_visual_words[ind])
                vlad[selected_visual_words] += diff
                return ind + 1, vlad

            ind_vlad = 0
            keep_going = lambda j, vlad: j < num_features
            vlad = np.zeros([self.codebook_size, self._feature_dimensionality],
                            dtype=np.float32)
            while keep_going(ind_vlad, vlad):
                _ConstructVladFromAssignments(ind_vlad, vlad)
                ind_vlad += 1

            vlad = np.reshape(vlad,
                              [self.codebook_size * self._feature_dimensionality])
            if use_l2_normalization:
                l2 = np.atleast_1d(np.linalg.norm(vlad, 2, 1))
                l2[l2 == 0] = 1
                vlad = vlad / np.expand_dims(l2, 1)

            return vlad

        return _ComputeVladNonEmptyFeatures()


class IndexIVFPQ(Index):
    def __init__(self, codebook_size, db_features, nb_features):
        Index.__init__(self, codebook_size, db_features, nb_features)
        self.k = 5
        self.size = 0
        self.codebook_size = codebook_size

    def train(self):
        features = np.array(self.db_features).astype('float32')
        coarse_quantizer = faiss.IndexFlatL2(features.shape[1])
        CODE_SIZE = 8  # Number of subquantizers, dimension % CODE_SIZE should be zero
        N_BITS = 4  # Bits per subquantizer
        self.index = faiss.IndexIVFPQ(coarse_quantizer, features.shape[1], self.codebook_size, CODE_SIZE, N_BITS)
        index_features = np.array(features).astype('float32')

        self.index.train(index_features)
        self.index.add(index_features)

        self.size = np.max(self.index_ownership) + 1

    def search(self, query_features):
        """
                Inputs:
        query_features (ndarray/list): NxD array or list of query node embeddings (features)
        """

        distances, matched_descriptors = self.index.search(query_features.astype('float32'), self.k)

        corresponding_images = self.index_ownership[matched_descriptors]

        sumofinliers = np.zeros(self.size)

        candidate_images, nboccurences = np.unique(corresponding_images, return_counts=True)
        for i, candidate in enumerate(candidate_images):
            sumofinliers[candidate] = nboccurences[i]

        ranks = np.argsort(sumofinliers)[::-1].astype(int)
        return ranks, sumofinliers


class IndexSMKb(Index):
    def __init__(self, codebook_size, db_features, nb_features):
        Index.__init__(self, codebook_size, db_features, nb_features)

    def train(self):
        index_features = np.array(self.db_features).astype('float32')

        DIMENSION_BINARY = 256
        M = np.random.randn(DIMENSION_BINARY, self.db_features.shape[1])
        Q, R = np.linalg.qr(M)
        P = Q[:DIMENSION_BINARY, :]

        self.cluster(index_features)

        self.size = np.max(self.index_ownership) + 1
        self.index_assignment = self.assign(index_features).squeeze()

        self.compute_idf()

        self.index_normalization = np.zeros(self.size)
        self.index_residuals = index_features - self.visual_words[self.index_assignment]
        l2_norm = np.sqrt(np.sum(np.power(self.index_residuals, 2), axis=1))
        self.index_residuals = self.index_residuals * 1/np.repeat(l2_norm[:, np.newaxis], repeats=index_features.shape[1], axis=1)
        for db_image in range(self.size):
            self.index_normalization[db_image] = self.compute_normalization_factor(
                index_features[self.index_ownership == db_image])

    def search(self, query_features):
        """
        Searches the database to get closest images
        :param query_features (ndarray/list): MxD array or list of query node embeddings (features)
        :return
        """
        query_features = np.array(query_features).astype('float32')
        query_normalization_factor = self.compute_normalization_factor(query_features)
        similarity = query_normalization_factor * self.measure_similarity(query_features)
        ranks = np.argsort(similarity)[::-1]
        return ranks, similarity[ranks]

    def measure_similarity(self, query_features):
        query_clusters = self.assign(query_features).squeeze()
        similarity = np.zeros(self.size)
        for c in np.unique(query_clusters):
            # Browse clusters and accumulate similarity with base images with the match kernel
            query_residuals = query_features[query_clusters == c] - self.visual_words[c]
            l2_norm = np.sqrt(np.sum(np.power(query_residuals, 2), axis=1))
            query_residuals /= np.repeat(l2_norm[:, np.newaxis], repeats=query_features.shape[1], axis=1)

            # Get database residuals of this cluster
            db_residuals = self.index_residuals[self.index_assignment == c]
            db_corresponding_images = self.index_ownership[self.index_assignment == c]

            # Compute match kernel output
            res = np.sum(self.selectivity_function(np.matmul(query_residuals, db_residuals.T)), axis=0)
            similarity[db_corresponding_images] += self.idf[c]*res

        similarity *= self.index_normalization
        return similarity

    def compute_normalization_factor(self, features):
        clusters = self.assign(features).squeeze()
        s = 0
        for c in np.unique(clusters):
            # Browse clusters and accumulate similarity with base images with the match kernel
            query_residuals = features[clusters == c] - self.visual_words[c]
            l2_norm = np.sqrt(np.sum(np.power(query_residuals, 2), axis=1))
            query_residuals /= np.repeat(l2_norm[:, np.newaxis], repeats=features.shape[1], axis=1)

            #  Compute match kernel output
            res = np.sum(self.selectivity_function(np.matmul(query_residuals, query_residuals.T)).squeeze())
            s += self.idf[c]*res

        return np.power(s, 0.5)


def gather_numpy(self, dim, index):
    """
    Gathers values along an axis specified by dim.
    For a 3-D tensor the output is specified by:
        out[i][j][k] = input[index[i][j][k]][j][k]  # if dim == 0
        out[i][j][k] = input[i][index[i][j][k]][k]  # if dim == 1
        out[i][j][k] = input[i][j][index[i][j][k]]  # if dim == 2

    :param dim: The axis along which to index
    :param index: A tensor of indices of elements to gather
    :return: tensor of gathered values
    """
    idx_xsection_shape = index.shape[:dim] + index.shape[dim + 1:]
    self_xsection_shape = self.shape[:dim] + self.shape[dim + 1:]
    if idx_xsection_shape != self_xsection_shape:
        raise ValueError("Except for dimension " + str(dim) +
                         ", all dimensions of index and self should be the same size")
    if index.dtype != np.dtype('int_'):
        raise TypeError("The values of index must be integers")
    data_swaped = np.swapaxes(self, 0, dim)
    index_swaped = np.swapaxes(index, 0, dim)
    gathered = np.choose(index_swaped, data_swaped)
    return np.swapaxes(gathered, 0, dim)


if __name__ == '__main__':

    codebook_size = 8000

    features = []
    nb_features = []
    for i in range(200):
        rd_nb_feats = np.random.randint(1, 1000)
        features.append(np.random.rand(rd_nb_feats, 128))
        nb_features.append(rd_nb_feats)

    nb_features = np.array(nb_features)
    features = np.concatenate(features, axis=0).astype(np.float32)

    #indexbow = IndexBoW(codebook_size=codebook_size, db_features=features, nb_features=nb_features)
    #indexvlad = IndexVLAD(codebook_size=codebook_size, db_features=features, nb_features=nb_features)
    #indexsmk = IndexSMK(codebook_size=codebook_size, db_features=features, nb_features=nb_features)
    indexasmk = IndexASMK(codebook_size=codebook_size, db_features=features, nb_features=nb_features)
    indexivfpq = IndexIVFPQ(codebook_size=codebook_size, db_features=features, nb_features=nb_features)
    indexasmkv2 = IndexASMKv2(codebook_size=codebook_size, db_features=features, nb_features=nb_features)
    #indexasmkv2_star = IndexASMKv2(codebook_size=codebook_size, db_features=features, nb_features=nb_features, type='asmk*')

    indexasmk.train()
    indexasmkv2.train()
    indexivfpq.train()

    #print(indexbow.search(query_features)[0])
    #print(indexvlad.search(query_features)[0])
    #print(indexsmk.search(query_features)[0])

    for i in range(1,10):
        query_features = features[np.sum(nb_features[:i-1]):np.sum(nb_features[:i-1])+nb_features[i]]
        print(indexasmk.search(query_features)[0][:4])
        print(indexivfpq.search(query_features)[0][:4])
        print(indexasmkv2.search(query_features)[0][:4])
        #print(indexasmkv2_star.search(query_features)[0][:4])

        print("--")





