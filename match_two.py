from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
#from util import input_fn, read_image_paths, display_correspondences  #extract_features
from feature_io import ReadAllFromFile
#import feature_extractor
from feature_matching import match_features, verify_RANSAC
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import cKDTree
from skimage.feature import plot_matches
from skimage import io
from skimage.measure import ransac
from skimage.transform import AffineTransform, ProjectiveTransform, warp, warp_coords
import tensorflow as tf
import feature_io
#from util import extract_features
import matplotlib.pyplot as plt
from PIL import Image

conf = None

_DISTANCE_THRESHOLD = 0.8



def compare_request_with_base(conf):
    tf.logging.set_verbosity(tf.logging.INFO)

    # Read features for the image base
    basefeatures = ReadAllFromFile(conf.basefeat_path)
    num_images_base = len(basefeatures)

    # Read list of source images for display
    baseimages = read_image_paths(conf.base_path)
    tf.logging.info("Loaded %d images to compare with request" % num_images_base)

    # Prepare feature extraction from request image
    # Tell TensorFlow that the model will be built into the default Graph.
    with tf.Graph().as_default():
        # Reading list of images.
        filename_queue = tf.train.string_input_producer([conf.request_path], shuffle=False)
        reader = tf.WholeFileReader()
        _, value = reader.read(filename_queue)
        image_tf = tf.image.decode_jpeg(value, channels=3)

        with tf.Session() as sess:
            # Initialize variables.
            init_op = tf.global_variables_initializer()
            sess.run(init_op)

            # Loading model that will be used.
            tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING],
                                       conf.model_path)
            graph = tf.get_default_graph()
            input_image = graph.get_tensor_by_name('input_image:0')
            input_score_threshold = graph.get_tensor_by_name('input_abs_thres:0')
            input_image_scales = graph.get_tensor_by_name('input_scales:0')
            input_max_feature_num = graph.get_tensor_by_name(
                'input_max_feature_num:0')
            boxes = graph.get_tensor_by_name('boxes:0')
            raw_descriptors = graph.get_tensor_by_name('features:0')
            feature_scales = graph.get_tensor_by_name('scales:0')
            attention_with_extra_dim = graph.get_tensor_by_name('scores:0')
            attention = tf.reshape(attention_with_extra_dim,
                                   [tf.shape(attention_with_extra_dim)[0]])

            locations, descriptors = feature_extractor.DelfFeaturePostProcessing(
                boxes, raw_descriptors, conf)

            # Start input enqueue threads.
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(sess=sess, coord=coord)

            # # Get image.
            im = sess.run(image_tf)

            # Extract features.
            (locations_req, descriptors_req, feature_scales_req,
             attention_req) = sess.run(
                [locations, descriptors, feature_scales, attention],
                feed_dict={
                    input_image:
                        im,
                    input_score_threshold:
                        conf.delf_local_config.score_threshold,
                    input_image_scales:
                        list(conf.image_scales),
                    input_max_feature_num:
                        conf.delf_local_config.max_feature_num
                })

            num_features_req = len(descriptors_req)

            # Finalize enqueue threads.
            coord.request_stop()
            coord.join(threads)

    for n in range(num_images_base):
        locations_tocompare, _, descriptors_tocompare, _, _ = basefeatures[n]
        num_features_2 = locations_tocompare.shape[0]
        tf.logging.info("Loaded image {}'s {} features".format(n, num_features_2))

        indices = match_features(descriptors_req, descriptors_tocompare, _DISTANCE_THRESHOLD)

        if np.min(indices) == num_features_req:
            print("No matches found !")
            continue

        # Select feature locations for putative matches.
        locations_2_to_use = np.array([
            locations_tocompare[i, ]
            for i in range(num_features_2)
            if indices[i] != num_features_req
        ])
        locations_1_to_use = np.array([
            locations_req[indices[i], ]
            for i in range(num_features_2)
            if indices[i] != num_features_req
        ])

        # Perform geometric verification using RANSAC.
        inliers = verify_RANSAC(locations_1_to_use, locations_2_to_use, AffineTransform)

        if inliers is None:
            print("No coherent geometric transformation found !")
            continue

        tf.logging.info('Found %d inliers' % sum(inliers))

        display_correspondences(conf.request_path, baseimages[n], locations_1_to_use, locations_2_to_use, inliers)

def match_two_no_plot(feat_path1, feat_path2):
    
    locations_1, _, descriptors_1, _, _ = feature_io.ReadFromFile(
        feat_path1)
    num_features_1 = locations_1.shape[0]
    locations_2, _, descriptors_2, _, _ = feature_io.ReadFromFile(
        feat_path2)
    num_features_2 = locations_2.shape[0]

    # Find nearest-neighbor matches using a KD tree.
    d1_tree = cKDTree(descriptors_1)
    _, indices = d1_tree.query(
        descriptors_2, distance_upper_bound=_DISTANCE_THRESHOLD)

    # Select feature locations for putative matches.
    locations_2_to_use = np.array([
        locations_2[i,]
        for i in range(num_features_2)
        if indices[i] != num_features_1
    ])
    locations_1_to_use = np.array([
        locations_1[indices[i],]
        for i in range(num_features_2)
        if indices[i] != num_features_1
    ])

    # Perform geometric verification using RANSAC.
    model, inliers = ransac(
        (locations_1_to_use, locations_2_to_use),
        AffineTransform,
        min_samples=int(min(locations_1_to_use.shape[0], locations_2_to_use.shape[0]) / 5),
        residual_threshold=20,
        max_trials=1000)

    return sum(inliers)


def match_two_single(conf):
    tf.logging.set_verbosity(tf.logging.INFO)

    # Get features
    data = extract_features(conf, [conf.image_1_path, conf.image_2_path])
    locations_1, _, descriptors_1, _, _ = data[0]
    num_features_1 = locations_1.shape[0]
    tf.logging.info("Loaded image 1's %d features" % num_features_1)
    locations_2, _, descriptors_2, _, _ = data[1]
    num_features_2 = locations_2.shape[0]
    tf.logging.info("Loaded image 2's %d features" % num_features_2)

    # Find nearest-neighbor matches using a KD tree.
    d1_tree = cKDTree(descriptors_1)
    _, indices = d1_tree.query(
        descriptors_2, distance_upper_bound=_DISTANCE_THRESHOLD)

    # Select feature locations for putative matches.
    locations_2_to_use = np.array([
        locations_2[i,]
        for i in range(num_features_2)
        if indices[i] != num_features_1
    ])
    locations_1_to_use = np.array([
        locations_1[indices[i],]
        for i in range(num_features_2)
        if indices[i] != num_features_1
    ])

    # Perform geometric verification using RANSAC.
    model, inliers = ransac(
        (locations_1_to_use, locations_2_to_use),
        AffineTransform,
        min_samples=int(min(locations_1_to_use.shape[0], locations_2_to_use.shape[0])/5),
        residual_threshold=20,
        max_trials=2000)

    tf.logging.info('Found %d inliers' % sum(inliers))
    tf.logging.info('Transform params :')
    tf.logging.info(model.params)

    # Check the transformation
    image1 = io.imread(conf.image_1_path, as_gray=True)
    image2 = io.imread(conf.image_2_path, as_gray=True)
    transform = AffineTransform(matrix=model.params)
    warpedimage = warp(image1, transform)
    warpedcoords = transform.params 

    fig = plt.figure(figsize=(3, 1))
    columns = 3
    rows = 1
    fig.add_subplot(rows, columns, 1)
    plt.imshow(image1, cmap='gray')
    fig.add_subplot(rows, columns, 2)
    plt.imshow(warpedimage, cmap='gray')
    fig.add_subplot(rows, columns, 3)
    plt.imshow(image2, cmap='gray')
    plt.show()

    # Visualize correspondences, and save to file.
    display_correspondences(conf.image_1_path, conf.image_2_path, locations_1_to_use, locations_2, inliers)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--image_1_path',
        type=str,
        default='test_images/image_1.jpg',
        help="""
      Path to test image 1.
      """)
    parser.add_argument(
        '--image_2_path',
        type=str,
        default='test_images/image_2.jpg',
        help="""
      Path to test image 2.
      """)
    parser.add_argument(
        '--config_path',
        type=str,
        default='delf_config_example.pbtxt',
        help="""
            Path to DelfConfig proto text file with configuration to be used for DELF extraction.
        """)
    args, unparsed = parser.parse_known_args()
    match_two_single(args)
