This project is a baseline for my thesis about Describing, Matching and Indexing images from different times and sources.

The code is for now largely based on Google's DELF algorithm and makes use of its DELF features, and protobuf implementation for storing them :

"Large-Scale Image Retrieval with Attentive Deep Local Features",
Hyeonwoo Noh, Andre Araujo, Jack Sim, Tobias Weyand, Bohyung Han,
Proc. ICCV'17# CBIR-baseline
