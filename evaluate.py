import numpy as np


def compute_ap(ranks, nres):
    """
    Computes average precision for given ranked indexes.
    
    Arguments
    ---------
    ranks : zerro-based ranks of positive images
    nres  : number of positive images
    
    Returns
    -------
    ap    : average precision
    """

    # number of images ranked by the system
    nimgranks = len(ranks)

    # accumulate trapezoids in PR-plot
    ap = 0

    recall_step = 1. / nres

    for j in np.arange(nimgranks):
        rank = ranks[j]

        if rank == 0:
            precision_0 = 1.
        else:
            precision_0 = float(j) / rank

        precision_1 = float(j + 1) / (rank + 1)

        ap += (precision_0 + precision_1) * recall_step / 2.

    return ap


def compute_map(ranks, gnd, kappas=[1, 5, 10]):
    """
    Computes the mAP for a given set of returned results.
         Usage:
           map = compute_map (ranks, gnd)
                 computes mean average precsion (map) only
           map, aps, pr, prs = compute_map (ranks, gnd, kappas)
                 computes mean average precision (map), average precision (aps) for each query
                 computes mean precision at kappas (pr), precision at kappas (prs) for each query
         Notes:
         1) ranks starts from 0, ranks.shape = db_size X #queries
         2) The ignore results (e.g., the query itself) should be declared in the gnd stuct array
         3) If there are no positive images for some query, that query is excluded from the evaluation
    """

    map = 0.
    nq = len(gnd)  # number of queries
    aps = np.zeros(nq)
    pr = np.zeros(len(kappas))
    prs = np.zeros((nq, len(kappas)))
    nempty = 0

    ranks = ranks.T
    for i in np.arange(nq):
        qgnd = np.atleast_1d(np.array(gnd[i]['positive'])).astype(int)

        # no positive images, skip from the average
        if qgnd.shape[0] == 0:
            aps[i] = float('nan')
            prs[i, :] = float('nan')
            nempty += 1
            continue

        try:
            qgndj = np.array(gnd[i]['ignore'])
        except:
            qgndj = np.empty(0)

        # sorted positions of positive and ignore images (0 based)
        pos = np.arange(ranks.shape[0])[np.in1d(ranks[:, i], qgnd)]
        ignore = np.arange(ranks.shape[0])[np.in1d(ranks[:, i], qgndj)]

        k = 0
        ij = 0
        if len(ignore):
            # decrease positions of positives based on the number of
            # ignore images appearing before them
            ip = 0
            while (ip < len(pos)):
                while (ij < len(ignore) and pos[ip] > ignore[ij]):
                    k += 1
                    ij += 1
                pos[ip] = pos[ip] - k
                ip += 1

        # compute ap
        ap = compute_ap(pos, len(qgnd))
        map = map + ap
        aps[i] = ap

        # compute precision @ k
        if pos.size != 0:
            pos += 1  # get it to 1-based
            for j in np.arange(len(kappas)):
                kq = min(max(pos), kappas[j])
                prs[i, j] = (pos <= kq).sum() / kq
            pr = pr + prs[i, :]

    if nq - nempty == 0:
        warnings.warn("mAP score can't be evaluated due to empty ground truth")
        return 0.0, aps, pr, prs

    map = map / (nq - nempty)
    pr = pr / (nq - nempty)

    return map, aps, pr, prs


def compute_map_per_class(ranks, gnd, classes=None, kappas=[]):
    """
    Computes the mAP per class.
    If we don't know the class per image, it can be guessed
    """

    maps = {}
    aps = {}

    if 'classes' is not None:
        unique_classes = np.unique(classes)
    else:
        #TODO
        raise NotImplementedError

    start = 0
    for currentclass in unique_classes:
        nb_queries = np.sum(classes == currentclass)
        if nb_queries == 0:
            continue
        classranks = ranks[:, start:start+nb_queries]
        maps[currentclass], aps[currentclass], _, _ = compute_map(classranks, gnd[start:start+nb_queries])
        start += nb_queries

    return maps, aps


def compute_map_and_print(dataset, ranks, gnd, kappas=[1, 5, 10], writer=None, epoch=0):

    # new evaluation protocol
    if dataset.startswith('roxford5k') or dataset.startswith('rparis6k'):
        
        gnd_t = []
        for i in range(len(gnd)):
            g = {}
            g['ok'] = np.concatenate([gnd[i]['easy']])
            g['junk'] = np.concatenate([gnd[i]['junk'], gnd[i]['hard']])
            gnd_t.append(g)
        mapE, apsE, mprE, prsE = compute_map(ranks, gnd_t, kappas)

        gnd_t = []
        for i in range(len(gnd)):
            g = {}
            g['ok'] = np.concatenate([gnd[i]['easy'], gnd[i]['hard']])
            g['junk'] = np.concatenate([gnd[i]['junk']])
            gnd_t.append(g)
        mapM, apsM, mprM, prsM = compute_map(ranks, gnd_t, kappas)

        gnd_t = []
        for i in range(len(gnd)):
            g = {}
            g['ok'] = np.concatenate([gnd[i]['hard']])
            g['junk'] = np.concatenate([gnd[i]['junk'], gnd[i]['easy']])
            gnd_t.append(g)
        mapH, apsH, mprH, prsH = compute_map(ranks, gnd_t, kappas)

        print('>> {}: mAP E: {}, M: {}, H: {}'.format(dataset, np.around(mapE*100, decimals=2), np.around(mapM*100, decimals=2), np.around(mapH*100, decimals=2)))
        print('>> {}: mP@k{} E: {}, M: {}, H: {}'.format(dataset, kappas, np.around(mprE*100, decimals=2), np.around(mprM*100, decimals=2), np.around(mprH*100, decimals=2)))

        if writer is not None:
            writer.add_scalar("test/mAP_{}_easy".format(dataset), mapE, epoch)
            writer.add_scalar("test/mAP_{}_medium".format(dataset), mapM, epoch)
            writer.add_scalar("test/mAP_{}_hard".format(dataset), mapH, epoch)

    # old evaluation protocol
    else:
        map, aps, _, _ = compute_map(ranks, gnd)
        print('>> {}: mAP {:.2f}'.format(dataset, np.around(map * 100, decimals=2)))

    if writer is not None:
        writer.add_scalar("test/mAP_{}".format(dataset), map, epoch)
